FROM tomcat

ARG SSH_PRIVATE_KEY

ARG SSH_PUBLIC_KEY
 
RUN apt-get update -y
 
RUN apt-get install -f
 
RUN apt-get install git -y
 
RUN apt-get install nodejs -y
 
RUN apt-get install npm -y

# Authorize SSH Host
RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    ssh-keyscan -t rsa bitbucket.org > /root/.ssh/known_hosts

# Add the keys and set permissions
RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa && \
    echo "${SSH_PUBLIC_KEY}" > /root/.ssh/id_rsa.pub && \
    chmod 600 /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa.pub && \
    cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys

RUN echo ${SSH_PRIVATE_KEY}

RUN cat /root/.ssh/id_rsa

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone git@bitbucket.org:BuckRogers_1/devops_g5.git

WORKDIR /tmp/build/devops_g5/

RUN chmod +x mvnw

RUN ./mvnw compile war:war

RUN cp target/training-1.0-SNAPSHOT.war  /usr/local/tomcat/webapps/

RUN rm /root/.ssh/id_rsa

RUN rm /root/.ssh/id_rsa.pub

EXPOSE 8080

