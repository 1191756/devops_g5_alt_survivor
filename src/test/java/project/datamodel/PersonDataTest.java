package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class PersonDataTest {
    @Test
    void getId() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        PersonID expected = new PersonID(mariaEmail);

        //Act
        PersonID result = personData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setId() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        Email fernandoEmail = new Email("fernando@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        PersonID expected = new PersonID(fernandoEmail);

        personData.setId(expected);

        //Act
        PersonID result = personData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getName() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        String expected = "Maria";
        //Act
        String result = personData.getName();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setName() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        String expected = "Mariachi";

        personData.setName(expected);

        //Act
        String result = personData.getName();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getBirthPlace() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        String expected = "Porto, Portugal";

        //Act
        String result = personData.getBirthPlace();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setBirthPlace() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        String expected = "Lisboa, Portugal";

        personData.setBirthPlace(expected);

        //Act
        String result = personData.getBirthPlace();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getBirthDate() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        String expected = "1978-06-21";

        //Act
        String result = personData.getBirthDate();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setBirthDate() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        String expected = "1982-12-24";

        personData.setBirthDate(expected);

        //Act
        String result = personData.getBirthDate();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getMother() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        personData.setMother("albertina@gmail.com");

        String expected = "albertina@gmail.com";

        //Act
        String result = personData.getMother();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setMother() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        personData.setMother("albertina@gmail.com");

        String expected = "albertinaalves@gmail.com";

        personData.setMother(expected);

        //Act
        String result = personData.getMother();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getFather() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        personData.setFather("alberto@gmail.com");

        String expected = "alberto@gmail.com";

        //Act
        String result = personData.getFather();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setFather() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        personData.setFather("alberto@gmail.com");

        String expected = "albertoalves@gmail.com";

        personData.setFather(expected);

        //Act
        String result = personData.getFather();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getSiblings() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        SiblingIDData siblingIDData = new SiblingIDData("rafael@gmail.com");

        personData.setSibling(siblingIDData);

        //Act
        List<SiblingIDData> result = personData.getSiblings();

        //Assert
        assertTrue(result.contains(siblingIDData));
    }

    @Test
    void setSiblings() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        SiblingIDData siblingIDData = new SiblingIDData("rafael@gmail.com");
        SiblingIDData siblingIDDataAlso = new SiblingIDData("diana@gmail.com");
        List<SiblingIDData> siblingIDDataList = new ArrayList<>();
        siblingIDDataList.add(siblingIDData);
        siblingIDDataList.add(siblingIDDataAlso);

        personData.setSiblings(siblingIDDataList);

        //Act
        List<SiblingIDData> result = personData.getSiblings();

        //Assert
        assertTrue(result.contains(siblingIDData));
        assertTrue(result.contains(siblingIDDataAlso));
    }

    @Test
    void setSibling() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        SiblingIDData siblingIDData = new SiblingIDData("rafael@gmail.com");
        SiblingIDData siblingIDDataAlso = new SiblingIDData("diana@gmail.com");

        personData.setSibling(siblingIDData);
        personData.setSibling(siblingIDDataAlso);

        //Act
        List<SiblingIDData> result = personData.getSiblings();

        //Assert
        assertTrue(result.contains(siblingIDData));
        assertTrue(result.contains(siblingIDDataAlso));
    }

    @Test
    void getCategories() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Beautiful Category", personData);
        personData.setCategory(personCategoryData);

        //Act
        List<PersonCategoryData> result = personData.getCategories();

        //Assert
        assertTrue(result.contains(personCategoryData));
    }

    @Test
    void setCategories() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Beautiful Category", personData);
        PersonCategoryData personCategoryDataAlso = new PersonCategoryData("Beautiful Category", personData);
        List<PersonCategoryData> personCategoryDataList = new ArrayList<>();

        personCategoryDataList.add(personCategoryData);
        personCategoryDataList.add(personCategoryDataAlso);

        personData.setCategories(personCategoryDataList);

        //Act
        List<PersonCategoryData> result = personData.getCategories();

        //Assert
        assertTrue(result.contains(personCategoryData));
        assertTrue(result.contains(personCategoryDataAlso));
    }

    @Test
    void setCategory() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Beautiful Category", personData);
        personData.setCategory(personCategoryData);

        //Act
        List<PersonCategoryData> result = personData.getCategories();

        //Assert
        assertTrue(result.contains(personCategoryData));
    }

    @Test
    void testEquals() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        //Act
        boolean result = personData.equals(personData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentObjectSameInfo() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonData personDataAlso = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        //Act
        boolean result = personData.equals(personDataAlso);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonData personDataAlso = null;

        //Act
        boolean result = personData.equals(personDataAlso);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObject() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        Description description = new Description("Mega description");

        //Act
        boolean result = personData.equals(description);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        int expected = Objects.hash(new PersonID(mariaEmail));

        //Act
        int result = personData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeFixedValue() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        int expected = -758711330;

        //Act
        int result = personData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");

        String expected = "PersonData{personId=maria@family.com, name='Maria', birthPlace='Porto, Portugal', birthDate='1978-06-21', mother='null', father='null', siblings=[], categories=[]}";

        //Act
        String result = personData.toString();

        //Assert
        assertEquals(expected, result);
    }
}