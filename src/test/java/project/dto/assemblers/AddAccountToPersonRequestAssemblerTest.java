package project.dto.assemblers;

import org.junit.jupiter.api.Test;
import project.dto.AddAccountToPersonRequestDTO;
import project.model.shared.Denomination;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AddAccountToPersonRequestAssemblerTest {

    @Test
    void addAccountMapToDTO() {
        AddAccountToPersonRequestDTO expected = new AddAccountToPersonRequestDTO(new Denomination("Groceries"), new Description("Fruit"), new PersonID(new Email("lia@gmail.com")));

        final String denomination = "Groceries";
        final String description = "Fruit";
        final String personEmail = "lia@gmail.com";

        AddAccountToPersonRequestDTO result = AddAccountToPersonRequestAssembler.addAccountMapToDTO(denomination, description, personEmail);

        assertEquals(expected.getPersonID(), result.getPersonID());
        assertEquals(expected.getDenomination(), result.getDenomination());
    }

    @Test
    void AddAccountToPersonRequestAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            AddAccountToPersonRequestAssembler addAccountToPersonRequestAssembler = new AddAccountToPersonRequestAssembler();
        });
    }
}