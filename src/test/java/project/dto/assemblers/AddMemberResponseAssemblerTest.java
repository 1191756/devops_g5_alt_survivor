package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.GroupDTO;
import project.dto.GroupResponseDTO;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AddMemberResponseAssemblerTest {
    Description groupDescription;
    Email email;
    PersonID responsibleId;
    Group switchGroup;
    GroupDTO groupDTO;

    @BeforeEach
    void setUp() {
        groupDescription = new Description("Family");
        email = new Email("student@gmail.com");
        responsibleId = new PersonID(email);

        switchGroup = new Group(groupDescription, responsibleId);

        groupDTO = GroupAssembler.mapToDTO(switchGroup);
    }


    @Test
    void mapToGroupResponseDTO() {
        //Arrange
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO("Family");
        //Act
        GroupResponseDTO result = AddMemberResponseAssembler.mapToDTO(groupDTO);

        //Assert
        assertEquals(groupResponseDTO, result);
    }

    @Test
    void AddMemberResponseAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            AddMemberResponseAssembler addMemberResponseAssembler = new AddMemberResponseAssembler();
        });
    }
}