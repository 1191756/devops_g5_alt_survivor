package project.dto.assemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.AddAccountToGroupRequestDTO;
import project.model.shared.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AddAccountToGroupRequestAssemblerTest {

    @Test
    @DisplayName("Add Account To Group Request Assembler Test - Happy path")
    void addAccountMapToDTOHappyPath() {
        //Arrange
        Denomination denomination = new Denomination("Dolls");
        Description description = new Description("Barbie");
        Description groupdescription = new Description("Girls  Group");
        Email responsibleEmail = new Email("sofia@gmail.com");
        PersonID personID = new PersonID(responsibleEmail);
        GroupID gpID = new GroupID(groupdescription);
        AddAccountToGroupRequestDTO expected = new AddAccountToGroupRequestDTO(denomination, description, personID, gpID);
        //Act

        AddAccountToGroupRequestDTO result = AddAccountToGroupRequestAssembler.mapToDTO("Dolls", "Barbie", "sofia@gmail.com", "Girls  Group");

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void addAccountToGroupRequestAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            AddAccountToGroupRequestAssembler addAccountToGroupRequestAssembler = new AddAccountToGroupRequestAssembler();
        });
    }

}