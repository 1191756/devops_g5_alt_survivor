package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddMemberRequestInfoDTOTest {
    AddMemberRequestInfoDTO addMemberRequestInfoDTO;
    String groupDescription;
    String personEmail;

    @BeforeEach
    void setUp() {
        // Arrange
        groupDescription = "Family";
        personEmail = "pedro@gmail.com";
        addMemberRequestInfoDTO = new AddMemberRequestInfoDTO(groupDescription, personEmail);
    }

    @DisplayName("Get Group Description")
    @Test
    void getGroupDescription() {
        String result = addMemberRequestInfoDTO.getGroupDescription();
    }

    @DisplayName("Set Group Description")
    @Test
    void setGroupDescription() {
        addMemberRequestInfoDTO.setGroupDescription("Family123");
    }

    @DisplayName("Get Person Email")
    @Test
    void getPersonEmail() {
        String result = addMemberRequestInfoDTO.getPersonEmail();
    }

    @DisplayName("Set Person Email")
    @Test
    void setPersonEmail() {
        addMemberRequestInfoDTO.setPersonEmail("marco@gmail.com");
    }

    @DisplayName("Override Equals - True")
    @Test
    void testEquals() {
        //Arrange
        AddMemberRequestInfoDTO addMemberRequestInfoDTO1 = new AddMemberRequestInfoDTO(groupDescription, personEmail);
        //Act
        boolean result = addMemberRequestInfoDTO1.equals(addMemberRequestInfoDTO);
        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Equals - Same Exact Object")
    @Test
    void testEqualsSameExactObject() {
        //Arrange
        AddMemberRequestInfoDTO addMemberRequestInfoDTO = new AddMemberRequestInfoDTO(groupDescription, personEmail);
        //Act
        boolean result = addMemberRequestInfoDTO.equals(addMemberRequestInfoDTO);
        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Equals - Different")
    @Test
    void testEqualsDifferent() {
        //Arrange
        AddMemberRequestInfoDTO addMemberRequestInfoDTO1 = new AddMemberRequestInfoDTO(groupDescription, "marco@gmail.com");
        AddMemberRequestInfoDTO addMemberRequestInfoDTO2 = new AddMemberRequestInfoDTO(groupDescription, personEmail);
        //Act
        boolean result = addMemberRequestInfoDTO1.equals(addMemberRequestInfoDTO2);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - Different group description")
    @Test
    void testEqualsDifferentGroup() {
        //Arrange
        AddMemberRequestInfoDTO addMemberRequestInfoDTO1 = new AddMemberRequestInfoDTO(groupDescription, personEmail);
        AddMemberRequestInfoDTO addMemberRequestInfoDTO2 = new AddMemberRequestInfoDTO("Family1", personEmail);
        //Act
        boolean result = addMemberRequestInfoDTO1.equals(addMemberRequestInfoDTO2);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - Differents")
    @Test
    void testEqualsDifferents() {
        //Arrange
        AddMemberRequestInfoDTO addMemberRequestInfoDTO1 = new AddMemberRequestInfoDTO("Family", "marco@gmail.com");
        AddMemberRequestInfoDTO addMemberRequestInfoDTO2 = new AddMemberRequestInfoDTO("Family1", "pedro@gmail.com");
        //Act
        boolean result = addMemberRequestInfoDTO1.equals(addMemberRequestInfoDTO2);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsFalse() {
        //Arrange
        AddMemberRequestInfoDTO addMemberRequestInfoDTO1 = new AddMemberRequestInfoDTO("Family", "marco@gmail.com");
        //Act
        boolean result = addMemberRequestInfoDTO1.equals(null);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsFalseAnotherObject() {
        //Arrange
        AddMemberRequestInfoDTO addMemberRequestInfoDTO1 = new AddMemberRequestInfoDTO("Family", "marco@gmail.com");
        //Act
        boolean result = addMemberRequestInfoDTO1.equals(personEmail);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - True")
    @Test
    void testEqualsTrue() {
        //Arrange
        AddMemberRequestInfoDTO addMemberRequestInfoDTO1 = new AddMemberRequestInfoDTO("Family", "marco@gmail.com");

        //Act
        boolean result = addMemberRequestInfoDTO1.equals(addMemberRequestInfoDTO1);
        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Hash Code - Not Equals")
    @Test
    void testHashCode() {
        //Arrange
        AddMemberRequestInfoDTO addMemberRequestInfoDTO1 = new AddMemberRequestInfoDTO("Family", "marco@gmail.com");
        AddMemberRequestInfoDTO addMemberRequestInfoDTO2 = new AddMemberRequestInfoDTO("Family1", "pedro@gmail.com");

        //Assert
        assertNotEquals(addMemberRequestInfoDTO1.hashCode(), addMemberRequestInfoDTO2.hashCode());
    }

    @DisplayName("Override Hash Code - Equals")
    @Test
    void testHashCodeEquals() {
        //Arrange
        AddMemberRequestInfoDTO addMemberRequestInfoDTO1 = new AddMemberRequestInfoDTO("Family", "marco@gmail.com");
        AddMemberRequestInfoDTO addMemberRequestInfoDTO2 = new AddMemberRequestInfoDTO("Family", "marco@gmail.com");

        //Assert
        assertEquals(addMemberRequestInfoDTO1.hashCode(), addMemberRequestInfoDTO2.hashCode());
    }
}