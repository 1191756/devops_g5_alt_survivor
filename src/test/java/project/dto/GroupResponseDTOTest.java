package project.dto;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.GroupResponseAssembler;

import static org.junit.jupiter.api.Assertions.*;

public class GroupResponseDTOTest {

    private final String groupDescription = "Group1";
    private final GroupResponseDTO groupResponseDTO = GroupResponseAssembler.mapToDTO(groupDescription);

    @Test
    void GroupDTOTest_Constructor() {
        new GroupResponseDTO(groupDescription);
    }

    @Test
    void getGroupDescription() {
        groupResponseDTO.getGroupDescription();
    }

    @Test
    void setGroupDescription() {
        groupResponseDTO.setGroupDescription("Group2");
    }


    @DisplayName("GroupResponseDTO - Equals - same object")
    @Test
    void GroupResponseDTO_Equals_same_object() {
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO(groupDescription);
        assertTrue(groupResponseDTO.equals(groupResponseDTO));
    }

    @DisplayName("GroupResponseDTO - Equals - Equal object")
    @Test
    void GroupResponseDTO_Equals_equal_object() {
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO(groupDescription);
        GroupResponseDTO groupResponseDTO2 = new GroupResponseDTO(groupDescription);
        assertTrue(groupResponseDTO.equals(groupResponseDTO2));
    }

    @DisplayName("AddGroupRequestDTO - Equals - object has different class")
    @Test
    void GroupResponseDTO_Equals_different_class() {
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO(groupDescription);
        Object o = new Object();
        assertFalse(groupResponseDTO.equals(o));
    }

    @DisplayName("AddGroupRequestDTO - Equals - object is different")
    @Test
    void GroupResponseDTO_Equals_different() {
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO(groupDescription);
        GroupResponseDTO groupResponseDTO2 = new GroupResponseDTO("TestGroup2");
        assertFalse(groupResponseDTO.equals(groupResponseDTO2));
    }

    @DisplayName("AddGroupRequestDTO - Equals - object is null")
    @Test
    void GroupResponseDTO_Equals_null() {
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO(groupDescription);
        assertFalse(groupResponseDTO.equals(null));
    }

    @DisplayName("Override Hash Code - Not Equals")
    @Test
    void testHashCode() {
        //Arrange
        GroupResponseDTO groupResponseDTO2 = new GroupResponseDTO("TestGroup2");
        //Assert
        assertNotEquals(groupResponseDTO.hashCode(), groupResponseDTO2.hashCode());
    }

    @DisplayName("Override Hash Code - Equals")
    @Test
    void testHashCodeEquals() {
        //Arrange
        GroupResponseDTO groupResponseDTO2 = new GroupResponseDTO(groupDescription);

        //Assert
        assertEquals(groupResponseDTO.hashCode(), groupResponseDTO2.hashCode());
    }
}
