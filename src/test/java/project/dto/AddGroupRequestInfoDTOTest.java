package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AddGroupRequestInfoDTOTest {

    @DisplayName("AddGroupRequestInfoDTO - Constructor")
    @Test
    void AddGroupRequestInfoDTO() {
        new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");
    }

    @DisplayName("AddGroupRequestInfoDTO - Coverage")
    @Test
    void AddGroupRequestInfoDTO_CoverageTests() {
        AddGroupRequestInfoDTO result = new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");
        result.getResponsibleEmail();
        result.setResponsibleEmail("testemail@gmail.com");
        result.getGroupDescription();
        result.setGroupDescription("TestGroup");
        result.hashCode();
    }

    @DisplayName("AddGroupRequestDTO - Equals - same object")
    @Test
    void AddGroupRequestInfoDTO_Equals_same_object() {
        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");
        assertTrue(addGroupRequestInfoDTO.equals(addGroupRequestInfoDTO));
    }

    @DisplayName("AddGroupRequestDTO - Equals - Equal object")
    @Test
    void AddGroupRequestInfoDTO_Equals_equal_object() {
        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");
        AddGroupRequestInfoDTO addGroupRequestInfoDTO2 = new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");
        assertTrue(addGroupRequestInfoDTO.equals(addGroupRequestInfoDTO2));
    }

    @DisplayName("AddGroupRequestDTO - Equals - object has different class")
    @Test
    void AddGroupRequestInfoDTO_Equals_different_class() {
        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");
        Object o = new Object();
        assertFalse(addGroupRequestInfoDTO.equals(o));
    }

    @DisplayName("AddGroupRequestDTO - Equals - object is different")
    @Test
    void AddGroupRequestInfoDTO_Equals_different() {
        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");
        AddGroupRequestInfoDTO addGroupRequestInfoDTO2 = new AddGroupRequestInfoDTO("Test Group2", "Testemail@gmail.com");
        assertFalse(addGroupRequestInfoDTO.equals(addGroupRequestInfoDTO2));
    }

    @DisplayName("AddGroupRequestDTO - Equals - object is different")
    @Test
    void AddGroupRequestInfoDTO_Equals_different2() {
        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");
        AddGroupRequestInfoDTO addGroupRequestInfoDTO2 = new AddGroupRequestInfoDTO("Test Group", "Testemail2@gmail.com");
        assertFalse(addGroupRequestInfoDTO.equals(addGroupRequestInfoDTO2));
    }

    @DisplayName("AddGroupRequestDTO - Equals - object is null")
    @Test
    void AddGroupRequestInfoDTO_Equals_null() {
        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");
        assertFalse(addGroupRequestInfoDTO.equals(null));
    }

    @DisplayName("Override Hash Code - Not Equals")
    @Test
    void testHashCode() {
        //Arrange
        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");
        AddGroupRequestInfoDTO addGroupRequestInfoDTO2 = new AddGroupRequestInfoDTO("Test Group2", "Testemail@gmail.com");
        //Assert
        assertNotEquals(addGroupRequestInfoDTO.hashCode(), addGroupRequestInfoDTO2.hashCode());
    }

    @DisplayName("Override Hash Code - Equals")
    @Test
    void testHashCodeEquals() {
        //Arrange
        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");
        AddGroupRequestInfoDTO addGroupRequestInfoDTO2 = new AddGroupRequestInfoDTO("Test Group", "Testemail@gmail.com");

        //Assert
        assertEquals(addGroupRequestInfoDTO.hashCode(), addGroupRequestInfoDTO2.hashCode());
    }
}