package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.*;

import static org.junit.jupiter.api.Assertions.*;

public class AddCategoryRequestDTOTest {
    Designation designation;
    GroupID groupDescription;
    Description description;
    PersonID responsibleEmail;
    Email email;
    AddCategoryRequestDTO addCategoryRequestDTO;

    @BeforeEach
    void setUp() {
        designation = new Designation("Equipamentos");
        description = new Description("Futebol Clube");
        groupDescription = new GroupID(description);
        email = new Email("ruifrederico@duasdaasneira.com");
        responsibleEmail = new PersonID(email);
        addCategoryRequestDTO = new AddCategoryRequestDTO(designation, groupDescription, responsibleEmail);
    }

    @Test
    void getDesignationTest() {
        addCategoryRequestDTO.getDesignation();
    }

    @Test
    void setDesignationTest() {
        addCategoryRequestDTO.setDesignation(designation);
    }

    @Test
    void getGroupDescriptionTest() {
        addCategoryRequestDTO.getGroupDescription();
    }

    @Test
    void setGroupDescriptionTest() {
        addCategoryRequestDTO.setGroupDescription(groupDescription);
    }

    @Test
    void getPersonIDTest() {
        addCategoryRequestDTO.getGroupDescription();
    }

    @Test
    void setPersonIDTest() {
        addCategoryRequestDTO.setResponsibleEmail(responsibleEmail);
    }

    @DisplayName("AddCategoryRequestDTOEqualsTrue - Testing Equals")
    @Test
    void TestEqualsTrue() {
        //Act
        AddCategoryRequestDTO expected = new AddCategoryRequestDTO(designation, groupDescription, responsibleEmail);
        AddCategoryRequestDTO result = addCategoryRequestDTO;
        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("AddCategoryRequestDTOEqualsTrue - Exact same object")
    @Test
    void TestEqualsExactSameObject() {
        //Act
        AddCategoryRequestDTO result = addCategoryRequestDTO;

        //Assert
        assertEquals(result, result);
    }

    @Test
    void TestEqualsFalse() {
        //Act
        designation = new Designation("Caça");
        AddCategoryRequestDTO expected = new AddCategoryRequestDTO(designation, groupDescription, responsibleEmail);
        AddCategoryRequestDTO result = addCategoryRequestDTO;
        //Assert
        assertNotEquals(expected, result);
    }

    @Test
    void TestEqualsDifferentPersonID() {
        //Arrange
        email = new Email("marcoantonio@romano.com");
        responsibleEmail = new PersonID(email);
        //Act
        AddCategoryRequestDTO expected = new AddCategoryRequestDTO(designation, groupDescription, responsibleEmail);
        AddCategoryRequestDTO result = addCategoryRequestDTO;
        //Assert
        assertNotEquals(expected, result);
    }

    @Test
    void TestEqualsDifferentDesignation() {
        //Arrange
        designation = new Designation("Chuteiras");
        //Act
        AddCategoryRequestDTO expected = new AddCategoryRequestDTO(designation, groupDescription, responsibleEmail);
        AddCategoryRequestDTO result = addCategoryRequestDTO;
        //Assert
        assertNotEquals(expected, result);
    }

    @Test
    void TestEqualsDifferentGroupID() {
        //Arrange
        description = new Description("Basquetebol Clube");
        groupDescription = new GroupID(description);
        //Act
        AddCategoryRequestDTO expected = new AddCategoryRequestDTO(designation, groupDescription, responsibleEmail);
        AddCategoryRequestDTO result = addCategoryRequestDTO;
        //Assert
        assertNotEquals(expected, result);
    }

    @Test
    void TestEqualsNullObjects() {
        assertNotNull(addCategoryRequestDTO);
    }

    @Test
    void testEqualsDifferentObjects() {
        description = new Description("Dance School");

        assertNotEquals(addCategoryRequestDTO, description);
    }

    @Test
    void testHashCode() {
        AddCategoryRequestDTO expected = new AddCategoryRequestDTO(designation, groupDescription, responsibleEmail);

        int result = addCategoryRequestDTO.hashCode();
        int expectedHash = expected.hashCode();

        assertEquals(result, expectedHash);
    }

    @Test
    void testHashCodeFixedValue() {
        int expectedHash = 1429641820;

        int result = addCategoryRequestDTO.hashCode();

        assertEquals(result, expectedHash);
    }
}