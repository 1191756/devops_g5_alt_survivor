package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.model.shared.Email;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class IsSiblingRequestInfoDTOTest {
    String personID1;
    String personID2;

    IsSiblingRequestInfoDTO isSiblingRequestInfoDTO;

    @BeforeEach
    void setUp() {
        personID1 = "duarte@stcp.com";
        personID2 = "marta@stcp.com";
        isSiblingRequestInfoDTO = new IsSiblingRequestInfoDTO(personID1, personID2);
    }

    @Test
    void getPersonID1() {
        isSiblingRequestInfoDTO.getPersonID1();
    }

    @Test
    void setIPersonID1() {
        isSiblingRequestInfoDTO.setPersonID1(isSiblingRequestInfoDTO.getPersonID1());
    }

    @Test
    void getPersonID2() {
        isSiblingRequestInfoDTO.getPersonID2();
    }

    @Test
    void setIPersonID2() {
        isSiblingRequestInfoDTO.setPersonID2(isSiblingRequestInfoDTO.getPersonID2());
    }

    @Test
    void testEquals() {
        //Arrange
        isSiblingRequestInfoDTO = new IsSiblingRequestInfoDTO(personID1, personID2);
        IsSiblingRequestInfoDTO isSiblingRequestInfoDTO1 = new IsSiblingRequestInfoDTO("duarte@stcp.com", "marta@stcp.com");

        // Act
        boolean result = isSiblingRequestInfoDTO.equals(isSiblingRequestInfoDTO1);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentPeople() {
        //Arrange
        isSiblingRequestInfoDTO = new IsSiblingRequestInfoDTO(personID1, personID2);
        IsSiblingRequestInfoDTO isSiblingRequestInfoDTO1 = new IsSiblingRequestInfoDTO("helder@stcp.com", "marta@stcp.com");

        // Act
        boolean result = isSiblingRequestInfoDTO.equals(isSiblingRequestInfoDTO1);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentobject() {
        //Arrange
        isSiblingRequestInfoDTO = new IsSiblingRequestInfoDTO(personID1, personID2);
        PersonID personID = new PersonID(new Email("helder@stcp.com"));
        // Act
        boolean result = isSiblingRequestInfoDTO.equals(personID);

        //Assert
        assertFalse(result);
    }


    @Test
    void testEqualsDifferentObject2() {
        //Arrange
        isSiblingRequestInfoDTO = new IsSiblingRequestInfoDTO(personID1, personID2);
        // Act
        boolean result = personID1.equals(personID2);

        //Assert
        assertFalse(result);
    }


    @Test
    void testEqualsSameObject() {
        //Arrange
        isSiblingRequestInfoDTO = new IsSiblingRequestInfoDTO(personID1, personID2);

        // Act
        boolean result = isSiblingRequestInfoDTO.equals(isSiblingRequestInfoDTO);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsSameObjectsPersons() {
        assertEquals(isSiblingRequestInfoDTO.getPersonID1(), personID1);
        assertEquals(isSiblingRequestInfoDTO.getPersonID2(), personID2);
    }

    @Test
    void testEqualsPersonNotEquals() {
        //Arrange
        isSiblingRequestInfoDTO = new IsSiblingRequestInfoDTO(personID1, personID2);
        IsSiblingRequestInfoDTO isSiblingRequestInfoDTO1 = new IsSiblingRequestInfoDTO("du@stcp.com", "ma@stcp.com");

        //Act/Assert

        assertNotEquals(personID1, isSiblingRequestInfoDTO1.getPersonID1());
        assertNotEquals(personID2, isSiblingRequestInfoDTO1.getPersonID2());
    }

    @Test
    void testHashCode() {
        IsSiblingRequestInfoDTO expect = new IsSiblingRequestInfoDTO("duarte@stcp.com", "marta@stcp.com");

        int expectHash = expect.hashCode();
        int resultHash = isSiblingRequestInfoDTO.hashCode();

        assertEquals(expectHash, resultHash);
    }

    @Test
    void testHashCodeDifferent() {
        IsSiblingRequestInfoDTO expect = new IsSiblingRequestInfoDTO("helder@stcp.com", "marta@stcp.com");

        int expectHash = expect.hashCode();
        int resultHash = isSiblingRequestInfoDTO.hashCode();

        assertNotEquals(expectHash, resultHash);
    }
}