package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.AccountResponseAssembler;
import project.model.shared.*;

import static org.junit.jupiter.api.Assertions.*;

class AccountResponseDTOTest {
    AccountResponseDTO accountResponseDTO;
    String denomination;
    String description;
    String ownerID;
    Denomination denominationD;
    PersonID personID;


    @BeforeEach
    void setUp() {
        // Arrange
        denomination = "School Account";
        description = "Dance School";
        ownerID = "rosalia@gmail.com";

        denominationD = new Denomination("School Account");
        personID = new PersonID(new Email("rosalia@gmail.com"));

        AccountDTO accountADTO = new AccountDTO();
        accountADTO.setDescription(new Description("Dance School"));
        accountADTO.setAccountID(new AccountID(denominationD, personID));
        accountResponseDTO = AccountResponseAssembler.mapToDTO(accountADTO);
    }

    @Test
    void getDenomination() {
        String result = accountResponseDTO.getDenomination();
    }

    @Test
    void setDenomination() {
        accountResponseDTO.setDenomination(denomination);
    }

    @Test
    void getDescription() {
        String result = accountResponseDTO.getDescription();
    }

    @Test
    void setDescription() {
        accountResponseDTO.setDescription(description);
    }

    @Test
    void getOwnerID() {
        String result = accountResponseDTO.getOwnerID();
    }

    @Test
    void setOwnerID() {
        accountResponseDTO.setOwnerID(ownerID);
    }

    @DisplayName("Hashcode")
    @Test
    void testHashCode() {

        AccountResponseDTO expect = new AccountResponseDTO("School Account", "Dance School", "rosalia@gmail.com");

        int expectHash = expect.hashCode();
        int resultHash = accountResponseDTO.hashCode();

        assertEquals(expectHash, resultHash);
    }

    @Test
    void testHashCodeNumber() {

        AccountResponseDTO expect = new AccountResponseDTO("School Account", "Dance School", "rosalia@gmail.com");

        int expectHash = expect.hashCode();
        int resultHash = 1765570825;

        assertEquals(expectHash, resultHash);
    }

    @DisplayName("Same Object - Override Equals")
    @Test
    void equalsSameObject() {
        assertTrue(accountResponseDTO.equals(accountResponseDTO));
    }

    @DisplayName("sameAttributes - Override Equals")
    @Test
    void equalsSameAttributes() {
        AccountResponseDTO accountResponseDTOFirst = new AccountResponseDTO("School Account", "Dance School", "rosalia@gmail.com");

        assertTrue(accountResponseDTOFirst.getOwnerID().equals(accountResponseDTO.getOwnerID()));
        assertTrue(accountResponseDTOFirst.getDescription().equals(accountResponseDTO.getDescription()));
        assertTrue(accountResponseDTOFirst.getDenomination().equals(accountResponseDTO.getDenomination()));
    }

    @DisplayName("Override Equals - objects with the same attributes")
    @Test
    void equalsSetIsAccountResponseDTOEquals() {
        //Arrange
        AccountResponseDTO accountResponseDTOFirst = new AccountResponseDTO("School Account", "Dance School", "rosalia@gmail.com");
        AccountResponseDTO accountResponseDTOSecond = new AccountResponseDTO("School Account", "Dance School", "rosalia@gmail.com");

        //Act//Assset
        assertTrue(accountResponseDTOFirst.equals(accountResponseDTOSecond));
    }

    @DisplayName("Override Equals - false")
    @Test
    void equalsSetIsAccountResponseDTOEqualsFalse() {
        //Arrange
        AccountResponseDTO accountResponseDTOFirst = new AccountResponseDTO("School Account", "Dance School", "rosalia@gmail.com");
        AccountResponseDTO accountResponseDTOSecond = new AccountResponseDTO("Groceries", "Dance School", "rosalia@gmail.com");
        //Act
        boolean result = accountResponseDTOFirst.equals(accountResponseDTOSecond);
        //Assset
        assertFalse(result);
    }

    @DisplayName("equals() - Object is null")
    @Test
    void equalsNullObject() {
        //Arrange
        AccountResponseDTO accountResponseDTO = new AccountResponseDTO("School Account", "Dance School", "rosalia@gmail.com");
        //Act// Assert
        assertFalse(accountResponseDTO.equals(null));
    }

    @DisplayName("Different Objects - Override Equals")
    @Test
    void equalsDifferentObjects() {
        assertFalse(accountResponseDTO.equals(description));
    }

    @DisplayName("equals() - diferent Object")
    @Test
    void diferentObject() {
        //Arrange
        Denomination differentObject = new Denomination("Cars");
        AccountResponseDTO accountResponseDTO = new AccountResponseDTO("Groceries Account", "Everything Juicy", "Good Girls");
        //Act
        boolean result = accountResponseDTO.equals(differentObject);

        // Assert
        assertFalse(result);
    }

    @DisplayName("test Equals - denomination")
    @Test
    void testEqualsDifferentObjectsDenomination() {

        //Arrange
        String denomination = "Party";
        String description = "Jeans";
        String ownerID = "marta@gmail.com";
        AccountResponseDTO accountResponseDTO1 = new AccountResponseDTO(denomination, description, ownerID);
        //Act
        String denomination2 = "Cub";
        AccountResponseDTO accountResponseDTO12 = new AccountResponseDTO(denomination2, description, ownerID);

        //Assert
        assertFalse(accountResponseDTO1.equals(accountResponseDTO12));
    }

    @DisplayName("test Equals - description")
    @Test
    void testEqualsDifferentObjectsDescription() {

        //Arrange
        String denomination = "Party";
        String description = "Jeans";
        String ownerID = "marta@gmail.com";
        AccountResponseDTO accountResponseDTO1 = new AccountResponseDTO(denomination, description, ownerID);
        //Act
        String description2 = "Shoes";
        AccountResponseDTO accountResponseDTO12 = new AccountResponseDTO(denomination, description2, ownerID);

        //Assert
        assertFalse(accountResponseDTO1.equals(accountResponseDTO12));
    }

    @DisplayName("test Equals - OwnerID")
    @Test
    void testEqualsDifferentObjectsOwnerID() {

        //Arrange
        String denomination = "Party";
        String description = "Jeans";
        String ownerID = "marta@gmail.com";
        AccountResponseDTO accountResponseDTO1 = new AccountResponseDTO(denomination, description, ownerID);
        //Act
        String ownerID2 = "joao@gmail.com";
        AccountResponseDTO accountResponseDTO12 = new AccountResponseDTO(denomination, description, ownerID2);

        //Assert
        assertFalse(accountResponseDTO1.equals(accountResponseDTO12));
    }
}