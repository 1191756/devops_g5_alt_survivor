package project;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;
import project.utils.ClockUtil;

@Profile("test2")
@Configuration
public class TestServerConfiguration {

    @Bean
    @Primary
    public GroupRepository groupMockRepository() {
        return Mockito.mock(GroupRepository.class);
    }

    @Bean
    @Primary
    public AccountRepository accountMockRepository() {
        return Mockito.mock(AccountRepository.class);
    }

    @Bean
    @Primary
    public PersonRepository personMockRepository() {
        return Mockito.mock(PersonRepository.class);
    }

    @Bean
    @Primary
    public LedgerRepository ledgerMockRepository() {
        return Mockito.mock(LedgerRepository.class);
    }

    @Bean
    @Primary
    public ClockUtil clockUtilMock() {
        return Mockito.mock(ClockUtil.class);
    }
}