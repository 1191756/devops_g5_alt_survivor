package project.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddTransactionToGroupRequestDTO;
import project.dto.TransactionDTO;
import project.model.group.Group;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;
import project.utils.ClockUtil;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
public class AddTransactionToGroupServiceTest {
    private final static LocalDateTime LOCAL_DATE_TIME = LocalDateTime.of(2020, 6, 2, 13, 0, 0, 0);
    @Autowired
    AddTransactionToGroupService addTransactionToGroupService;
    @Autowired
    GroupRepository groupMockRepository;
    @Autowired
    LedgerRepository ledgerMockRepository;
    @Autowired
    PersonRepository personMockRepository;
    @Autowired
    ClockUtil clockUtilMock;
    Clock clock;
    Clock fixedClock;

    //BeforeEach
    Person person1;
    Person person2;
    Category category1;
    Category category2;
    Group group1;
    Group group2;
    Ledger ledger1;
    AccountID debit1;
    AccountID debit2;
    AccountID credit1;
    AccountID credit2;
    Transaction transaction1;
    Transaction transaction2;
    Transaction transaction3;
    Transaction transaction4;
    Description description1;
    Description description2;
    Address birthAddress1;
    Address birthAddress2;
    Date birthdate1;
    Date birthdate2;
    Name name1;
    Name name2;
    Email email1;
    Email email2;

    @BeforeEach
    void setUpForTests() {
        clock = Clock.systemDefaultZone();
        fixedClock = Clock.fixed(LOCAL_DATE_TIME.toInstant(ZoneOffset.UTC), ZoneId.systemDefault());

        //Person1
        birthAddress1 = new Address("Maia, Portugal");
        birthdate1 = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        name1 = new Name("Ricardo Carvalho");
        email1 = new Email("ricardo@family.com");
        person1 = new Person(name1, birthAddress1, birthdate1, email1, null, null);

        //Person2
        birthAddress2 = new Address("Maia, Portugal");
        birthdate2 = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        name2 = new Name("Vitor Carvalho");
        email2 = new Email("vitor@family.com");
        person2 = new Person(name2, birthAddress2, birthdate2, email2, null, null);

        //Categories
        category1 = new Category(new Designation("Shopping"));
        category2 = new Category(new Designation("Services"));

        //Group1
        description1 = new Description("FantasticGroup");

        group1 = new Group(description1, person1.getPersonID());

        group1.addCategory(category1);
        group1.addCategory(category2);

        //Group2
        description2 = new Description("AmazingGroup");

        group2 = new Group(description2, person2.getPersonID());

        group2.addCategory(category1);
        group2.addCategory(category2);

        //Ledger1
        ledger1 = new Ledger(new LedgerID(new OwnerID(group1.getID().getId())));

        //Accounts
        debit1 = new AccountID(new Denomination("Primary account"), group1.getID());
        credit1 = new AccountID(new Denomination("Secondary account"), group1.getID());

        debit2 = new AccountID(new Denomination("home account"), group2.getID());
        credit2 = new AccountID(new Denomination("business account"), group2.getID());

        //Transaction1
        transaction1 = new Transaction(100.0,
                new Description("simulated transaction"),
                new TransactionDate(LocalDateTime.now(fixedClock)),
                category1,
                debit1,
                credit1,
                TransactionType.valueOf("CREDIT"));

        //Transaction2
        transaction2 = new Transaction(5.0,
                new Description("small transaction"),
                new TransactionDate(LocalDateTime.now()),
                category2,
                debit1,
                credit1,
                TransactionType.valueOf("DEBIT"));

        //Transaction3
        transaction3 = new Transaction(380.5,
                new Description("big transaction"),
                new TransactionDate(LocalDateTime.now()),
                category1,
                debit1,
                credit1,
                TransactionType.valueOf("CREDIT"));

        //Transaction4
        transaction4 = new Transaction(3.0,
                new Description("small transaction"),
                new TransactionDate(LocalDateTime.now()),
                category2,
                debit1,
                credit1,
                TransactionType.valueOf("DEBIT"));
    }

    @DisplayName("AddTransactionToGroup() - Happy path")
    @Test
    public void addTransactionToGroup_HappyPath() {
        //Arrange

        person1 = new Person(name1, birthAddress1, birthdate1, email1, null, null);
        group1 = new Group(description1, person1.getPersonID());

        group1.addCategory(category1);
        group1.addCategory(category2);


        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO;
        addTransactionToGroupRequestDTO = new AddTransactionToGroupRequestDTO(person1.getPersonID(),
                group1.getID(),
                transaction1.getAmount(),
                transaction1.getDescription(),
                transaction1.getCategory(),
                transaction1.getDebit(),
                transaction1.getCredit(),
                transaction1.getType());

        Mockito.when(groupMockRepository.findById(addTransactionToGroupRequestDTO.getGroupID())).thenReturn(Optional.of(group1));
        Mockito.when(personMockRepository.findById(person1.getPersonID())).thenReturn(Optional.of(person1));
        Mockito.when(ledgerMockRepository.findById(new LedgerID(new OwnerID(addTransactionToGroupRequestDTO.getGroupID().getId())))).thenReturn(Optional.of(ledger1));
        Mockito.when(ledgerMockRepository.save(ledger1)).thenReturn(ledger1);
        Mockito.when(clockUtilMock.clock()).thenReturn(fixedClock);

        TransactionDTO expectedTransactionDTO = new TransactionDTO(transaction1.getAmount(),
                transaction1.getDescription(),
                transaction1.getDate(),
                transaction1.getCategory(),
                transaction1.getDebit(),
                transaction1.getCredit(),
                transaction1.getType()
        );

        //Act
        TransactionDTO resultDTO = addTransactionToGroupService.addTransactionToGroup(addTransactionToGroupRequestDTO);

        //Assert
        assertEquals(expectedTransactionDTO, resultDTO);
    }
}
