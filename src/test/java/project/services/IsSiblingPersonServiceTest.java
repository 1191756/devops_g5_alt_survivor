package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.IsSiblingRequestDTO;
import project.dto.assemblers.IsSiblingRequestDTOAssembler;
import project.exceptions.NotFoundException;
import project.model.person.Person;
import project.model.shared.Address;
import project.model.shared.Date;
import project.model.shared.Email;
import project.model.shared.Name;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
class IsSiblingPersonServiceTest {
    Address birthAddress = new Address("Porto, Portugal");
    Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
    Person father;
    Person mother;
    Person son;
    Person sibling;
    Person notSibling;
    Person anotherFather;
    Person anotherMother;
    Person sameMotherSon;
    Person sameMotherSibling;
    Person sameFatherSon;
    Person sameFatherSibling;
    Person listSon;
    Person listSibling;
    Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
    @Autowired
    private PersonRepository personMockRepository;
    @Autowired
    private IsSiblingPersonService isSiblingPersonService;

    /**
     * User Story 1 (US1):
     * Como gestor de sistema, quero saber se determinada pessoa é irmão/irmã de outra. É irmão/irmã de outra se tem
     * a mesma mãe ou o mesmo pai, ou se na lista de irmãos está incluída a outra. A relação de irmão é bidirecional,
     * i.e. se A é irmão de B, então B é irmão de A.
     */
    @DisplayName("isSibling() - Verifies if other Person has same parents, or if it is located inside sibling's Set")
    @Test
    void isSiblingTrueSameMotherAndFather() {
        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisio@family.com");
        son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nuno@family.com");
        sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());

        IsSiblingRequestDTO isSiblingRequestDTO = IsSiblingRequestDTOAssembler.mapToDTO("tarcisio@family.com", "nuno@family.com");

        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID1())).thenReturn(Optional.of(son));
        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID2())).thenReturn(Optional.of(sibling));

        //Act
        boolean result = isSiblingPersonService.isSiblingPerson(isSiblingRequestDTO);

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - True if other Person has same mother")
    @Test
    void isSiblingTrueSameMother() {
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        Name ana = new Name("Ana");
        Email anaEmail = new Email("ana@family.com");
        anotherMother = new Person(ana, birthAddress, parentsBirthdate, anaEmail, null, null);

        Name luis = new Name("Luis");
        Email luisEmail = new Email("luis@family.com");
        anotherFather = new Person(luis, birthAddress, parentsBirthdate, luisEmail, null, null);

        //Same mother siblings
        Name diogo = new Name("Diogo");
        Email diogoEmail = new Email("diogo@family.com");
        sameMotherSon = new Person(diogo, birthAddress, personsBirthdate, diogoEmail, anotherMother.getPersonID(), anotherFather.getPersonID());

        Name alexandre = new Name("Alexandre");
        Email alexandreEmail = new Email("alexandre@family.com");
        sameMotherSibling = new Person(alexandre, birthAddress, personsBirthdate, alexandreEmail, anotherMother.getPersonID(), father.getPersonID());

        IsSiblingRequestDTO isSiblingRequestDTO = new IsSiblingRequestDTO("diogo@family.com", "alexandre@family.com");

        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID1())).thenReturn(Optional.of(sameMotherSon));
        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID2())).thenReturn(Optional.of(sameMotherSibling));

        //Act
        boolean result = isSiblingPersonService.isSiblingPerson(isSiblingRequestDTO);

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - True if other Person has same father")
    @Test
    void isSiblingTrueSameFather() {
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);

        Name ana = new Name("Ana");
        Email anaEmail = new Email("ana@family.com");
        anotherMother = new Person(ana, birthAddress, parentsBirthdate, anaEmail, null, null);

        Name luis = new Name("Luis");
        Email luisEmail = new Email("luis@family.com");
        anotherFather = new Person(luis, birthAddress, parentsBirthdate, luisEmail, null, null);

        //Same father siblings
        Name duarte = new Name("Duarte");
        Email duarteEmail = new Email("duarte@family.com");
        sameFatherSon = new Person(duarte, birthAddress, personsBirthdate, duarteEmail, anotherMother.getPersonID(), anotherFather.getPersonID());
        Name rui = new Name("Rui");
        Email ruiEmail = new Email("rui@family.com");
        sameFatherSibling = new Person(rui, birthAddress, personsBirthdate, ruiEmail, mother.getPersonID(), anotherFather.getPersonID());

        IsSiblingRequestDTO isSiblingRequestDTO = new IsSiblingRequestDTO("duarte@family.com", "rui@family.com");

        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID1())).thenReturn(Optional.of(sameFatherSon));
        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID2())).thenReturn(Optional.of(sameFatherSibling));

        //Act
        boolean result = isSiblingPersonService.isSiblingPerson(isSiblingRequestDTO);

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - True if other Person is inside sibling's list")
    @Test
    void isSiblingTrueSiblings() {
        //Persons with the sibling in the siblings list
        Name antonio = new Name("António");
        Email antonioEmail = new Email("antonio@family.com");
        listSon = new Person(antonio, birthAddress, personsBirthdate, antonioEmail, null, null);
        Name frederico = new Name("Frederico");
        Email fredericoEmail = new Email("frederico@family.com");
        listSibling = new Person(frederico, birthAddress, personsBirthdate, fredericoEmail, null, null);

        listSon.addSibling(listSibling.getPersonID());

        IsSiblingRequestDTO isSiblingRequestDTO = new IsSiblingRequestDTO("antonio@family.com", "frederico@family.com");

        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID1())).thenReturn(Optional.of(listSon));
        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID2())).thenReturn(Optional.of(listSibling));

        //Act
        boolean result = isSiblingPersonService.isSiblingPerson(isSiblingRequestDTO);

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - False if other Person is not sibling")
    @Test
    void isSiblingFalse() {
        // No family ties person
        Name mafalda = new Name("Mafalda");
        Email mafaldaEmail = new Email("mafalda@family.com");
        notSibling = new Person(mafalda, birthAddress, personsBirthdate, mafaldaEmail, null, null);

        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisio@family.com");
        son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());


        IsSiblingRequestDTO isSiblingRequestDTO = new IsSiblingRequestDTO("mafalda@family.com", "tarcisio@family.com");

        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID1())).thenReturn(Optional.of(notSibling));
        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID2())).thenReturn(Optional.of(son));

        //Act
        boolean result = isSiblingPersonService.isSiblingPerson(isSiblingRequestDTO);

        //Assert
        assertFalse(result);
    }

    @DisplayName("isSibling() - No person in repository for main person ID")
    @Test
    void isSiblingSonIdNotInRepository() {
        //Arrange
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisio@family.com");
        son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());

        IsSiblingRequestDTO isSiblingRequestDTO = new IsSiblingRequestDTO("unexistentPerson@family.com", "tarcisio@family.com");

        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID1())).thenReturn(Optional.empty());
        Mockito.when(personMockRepository.findById(isSiblingRequestDTO.getPersonID2())).thenReturn(Optional.of(son));

        //Act//Assert
        assertThrows(NotFoundException.class, () -> {
            boolean result = isSiblingPersonService.isSiblingPerson(isSiblingRequestDTO);
        });
    }
}