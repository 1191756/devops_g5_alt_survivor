package project;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("exceptions")
@Configuration
public class ExceptionTestConfiguration {
    @Bean
    @Primary
    public NullPointerException nullPointerException() {
        return Mockito.mock(NullPointerException.class);
    }
}


