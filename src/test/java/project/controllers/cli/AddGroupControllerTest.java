package project.controllers.cli;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddGroupRequestDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.AddGroupRequestAssembler;
import project.dto.assemblers.GroupAssembler;
import project.exceptions.AlreadyExistsException;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.GroupID;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;
import project.services.AddGroupService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class AddGroupControllerTest {

    @Autowired
    private AddGroupController addGroupController;

    @Autowired
    private AddGroupService addGroupMockService;

    @Autowired
    private GroupRepository groupRepository;

    @DisplayName("As a user, I want to create a group, becoming a group administrator -add Group - Happy path ")
    @Test
    void addGroupHappyPath() {
        //Arrange
        Description description = new Description("HappyGroup");
        Email email = new Email("maria@gmail.com");
        PersonID responsibleID = new PersonID(email);
        Group newGroup = new Group(description, responsibleID);
        GroupDTO expected = GroupAssembler.mapToDTO(newGroup);
        expected.setGroupID(new GroupID(description));

        AddGroupRequestDTO addGroupRequestDTO = AddGroupRequestAssembler.mapToDTO(description.getDescription(), email.getEmail());

        Mockito.when(addGroupMockService.addGroup(addGroupRequestDTO)).thenReturn(expected);

        //Act
        GroupDTO result = addGroupController.addGroup(description.getDescription(), email.getEmail());

        //Assert
        assertEquals(result, expected);
    }

    @DisplayName("Assert that a specific exception is thrown when the group already exists")
    @Test
    void addGroup_ExistingGroup() {
        //Arrange
        Description description2 = new Description("Group2");
        Email email2 = new Email("maria2@gmail.com");
        PersonID responsible2 = new PersonID(email2);

        Group group2 = new Group(description2, responsible2);
        groupRepository.save(group2);

        AddGroupRequestDTO addGroupRequestDTO2 = new AddGroupRequestDTO(description2, new PersonID(email2));
        Mockito.when(addGroupMockService.addGroup(addGroupRequestDTO2)).thenThrow(new AlreadyExistsException());
        //Act//Assert

        Exception exception = assertThrows(AlreadyExistsException.class, () -> {
            GroupDTO result = addGroupController.addGroup(description2.getDescription(), email2.getEmail());
        });
    }
}