package project.controllers.cli;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.IsSiblingRequestDTO;
import project.dto.assemblers.IsSiblingRequestDTOAssembler;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.PersonRepository;
import project.services.IsSiblingPersonService;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class IsSiblingPersonControllerTest {
    @Autowired
    private IsSiblingPersonService isSiblingPersonMockService;
    @Autowired
    private IsSiblingPersonController isSiblingPersonController;
    @Autowired
    private PersonRepository personRepository;

    Person father;
    Person mother;
    Person son;
    Person sibling;
    Person notSibling;
    Person anotherFather;
    Person anotherMother;
    Person sameMotherSon;
    Person sameMotherSibling;
    Person sameFatherSon;
    Person sameFatherSibling;
    Person listSon;
    Person listSibling;

    @BeforeEach
    void setUp() {

        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisio@family.com");
        son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nuno@family.com");
        sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());

        //Extra Mother and Father
        Name ana = new Name("Ana");
        Email anaEmail = new Email("ana@family.com");
        anotherMother = new Person(ana, birthAddress, parentsBirthdate, anaEmail, null, null);
        Name luis = new Name("Luis");
        Email luisEmail = new Email("luis@family.com");
        anotherFather = new Person(luis, birthAddress, parentsBirthdate, luisEmail, null, null);

        //Same mother siblings
        Name diogo = new Name("Diogo");
        Email diogoEmail = new Email("diogo@family.com");
        sameMotherSon = new Person(diogo, birthAddress, personsBirthdate, diogoEmail, anotherMother.getPersonID(), anotherFather.getPersonID());
        Name alexandre = new Name("Alexandre");
        Email alexandreEmail = new Email("alexandre@family.com");
        sameMotherSibling = new Person(alexandre, birthAddress, personsBirthdate, alexandreEmail, anotherMother.getPersonID(), father.getPersonID());

        //Same father siblings
        Name duarte = new Name("Duarte");
        Email duarteEmail = new Email("duarte@family.com");
        sameFatherSon = new Person(duarte, birthAddress, personsBirthdate, duarteEmail, anotherMother.getPersonID(), anotherFather.getPersonID());
        Name rui = new Name("Rui");
        Email ruiEmail = new Email("rui@family.com");
        sameFatherSibling = new Person(rui, birthAddress, personsBirthdate, ruiEmail, mother.getPersonID(), anotherFather.getPersonID());

        //Persons with the sibling in the siblings list
        Name antonio = new Name("António");
        Email antonioEmail = new Email("antonio@family.com");
        listSon = new Person(antonio, birthAddress, personsBirthdate, antonioEmail, null, null);
        Name frederico = new Name("Frederico");
        Email fredericoEmail = new Email("frederico@family.com");
        listSibling = new Person(frederico, birthAddress, personsBirthdate, fredericoEmail, null, null);

        listSon.addSibling(listSibling.getPersonID());
        listSibling.addSibling(listSon.getPersonID());

        // No family ties person
        Name mafalda = new Name("Mafalda");
        Email mafaldaEmail = new Email("mafalda@family.com");
        notSibling = new Person(mafalda, birthAddress, personsBirthdate, mafaldaEmail, null, null);

        personRepository.save(mother);
        personRepository.save(father);
        personRepository.save(son);
        personRepository.save(sibling);
        personRepository.save(notSibling);
        personRepository.save(anotherFather);
        personRepository.save(anotherMother);
        personRepository.save(sameMotherSon);
        personRepository.save(sameMotherSibling);
        personRepository.save(sameFatherSon);
        personRepository.save(sameFatherSibling);
        personRepository.save(listSon);
        personRepository.save(listSibling);

    }

    /**
     * User Story 1 (US1):
     * Como gestor de sistema, quero saber se determinada pessoa é irmão/irmã de outra. É irmão/irmã de outra se tem
     * a mesma mãe ou o mesmo pai, ou se na lista de irmãos está incluída a outra. A relação de irmão é bidirecional,
     * i.e. se A é irmão de B, então B é irmão de A.
     */
    @DisplayName("isSibling() - Verifies if other Person has same parents, or if it is located inside sibling's Set")
    @Test
    void isSiblingTrueSameMotherAndFather() {
        IsSiblingRequestDTO isSiblingRequestDTO = IsSiblingRequestDTOAssembler.mapToDTO(son.getPersonID().toString(), sibling.getPersonID().toString());
        Mockito.when(isSiblingPersonMockService.isSiblingPerson(isSiblingRequestDTO)).thenReturn(true);

        //Act
        boolean result = isSiblingPersonController.isSibling(son.getPersonID().toString(), sibling.getPersonID().toString());

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - True if other Person has same mother")
    @Test
    void isSiblingTrueSameMother() {
        IsSiblingRequestDTO isSiblingRequestDTO = IsSiblingRequestDTOAssembler.mapToDTO(sameMotherSon.getPersonID().toString(), sameMotherSibling.getPersonID().toString());
        Mockito.when(isSiblingPersonMockService.isSiblingPerson(isSiblingRequestDTO)).thenReturn(true);

        //Act
        boolean result = isSiblingPersonController.isSibling(sameMotherSon.getPersonID().toString(), sameMotherSibling.getPersonID().toString());

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - True if other Person has same father")
    @Test
    void isSiblingTrueSameFather() {
        IsSiblingRequestDTO isSiblingRequestDTO = IsSiblingRequestDTOAssembler.mapToDTO(sameFatherSon.getPersonID().toString(), sameFatherSibling.getPersonID().toString());
        Mockito.when(isSiblingPersonMockService.isSiblingPerson(isSiblingRequestDTO)).thenReturn(true);

        //Act
        boolean result = isSiblingPersonController.isSibling(sameFatherSon.getPersonID().toString(), sameFatherSibling.getPersonID().toString());

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - True if other Person is inside sibling's list")
    @Test
    void isSiblingTrueSiblings() {

        IsSiblingRequestDTO isSiblingRequestDTO = IsSiblingRequestDTOAssembler.mapToDTO(listSon.getPersonID().toString(), listSibling.getPersonID().toString());
        Mockito.when(isSiblingPersonMockService.isSiblingPerson(isSiblingRequestDTO)).thenReturn(true);

        //Act
        boolean result = isSiblingPersonController.isSibling(listSon.getPersonID().toString(), listSibling.getPersonID().toString());

        //Assert
        assertTrue(result);

        IsSiblingRequestDTO isSiblingSecondRequestDTO = IsSiblingRequestDTOAssembler.mapToDTO(listSibling.getPersonID().toString(), listSon.getPersonID().toString());
        Mockito.when(isSiblingPersonMockService.isSiblingPerson(isSiblingSecondRequestDTO)).thenReturn(true);

        //Act
        result = isSiblingPersonController.isSibling(listSibling.getPersonID().toString(), listSon.getPersonID().toString());

        //Assert
        assertTrue(result);
    }

    @DisplayName("isSibling() - False if other Person is not sibling")
    @Test
    void isSiblingFalse() {
        //Act
        boolean result = isSiblingPersonController.isSibling(son.getPersonID().toString(), notSibling.getPersonID().toString());

        //Assert
        assertFalse(result);
    }

    @DisplayName("isSibling() - False if other Person is null ")
    @Test
    void isSiblingTrueSiblingsNull() {
        //Arrange
        Person nullPerson = null;

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            boolean result = isSiblingPersonController.isSibling(son.getPersonID().toString(), nullPerson.getPersonID().toString());
        });
    }

    @DisplayName("isSibling() - No person in repository for main person ID")
    @Test
    void isSiblingSonIdNotInRepository() {
        //Arrange
        Email unexistentPersonEmail = new Email("unexistentPerson@family.com");
        PersonID unexistentPersonID = new PersonID(unexistentPersonEmail);

        IsSiblingRequestDTO isSiblingRequestDTO = IsSiblingRequestDTOAssembler.mapToDTO(unexistentPersonID.toString(), sibling.getPersonID().toString());
        Mockito.when(isSiblingPersonMockService.isSiblingPerson(isSiblingRequestDTO)).thenThrow(NoSuchElementException.class);

        //Act//Assert
        assertThrows(NoSuchElementException.class, () -> {
            boolean result = isSiblingPersonController.isSibling(unexistentPersonID.toString(), sibling.getPersonID().toString());
        });
    }

    @DisplayName("isSibling() - No person in repository for second person ID")
    @Test
    void isSiblingSiblingIdNotInRepository() {
        //Arrange
        Email unexistentPersonEmail = new Email("unexistentPerson@family.com");
        PersonID unexistentPersonID = new PersonID(unexistentPersonEmail);

        IsSiblingRequestDTO isSiblingRequestDTO = IsSiblingRequestDTOAssembler.mapToDTO(son.getPersonID().toString(), unexistentPersonID.toString());
        Mockito.when(isSiblingPersonMockService.isSiblingPerson(isSiblingRequestDTO)).thenThrow(NoSuchElementException.class);

        //Act//Assert
        assertThrows(NoSuchElementException.class, () -> {
            boolean result = isSiblingPersonController.isSibling(son.getPersonID().toString(), unexistentPersonID.toString());
        });
    }
}