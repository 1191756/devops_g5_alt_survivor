package project.controllers.web.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.repositories.PersonRepository;
import project.utils.GetJsonNodeValue;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class IsSiblingPersonRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    PersonRepository personRepository;


    @DisplayName("isSiblingPerson - Happy Path")
    @Test
    void isSiblingPersonHappyPath() throws Exception {
        //Arrange
        final String uri = "/persons/nuno@family.com/siblings/tarcisio@family.com";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("{\"isSibling\":\"true\"}", content);
    }


    @DisplayName("isSiblingPerson - No user found(first person)")
    @Test
    void isSiblingPersonNoUSerFoundFirstPerson() throws Exception {
        //Arrange
        final String uri = "/persons/nu@family.com/siblings/tarcisio@family.com";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("Please select an existing Element", GetJsonNodeValue.nodeAsString(content, "message"));
    }


    @DisplayName("isSiblingPerson - No user found(second person)")
    @Test
    void isSiblingPersonNoUSerFoundSecondPerson() throws Exception {
        //Arrange
        final String uri = "/persons/nuno@family.com/siblings/tar@family.com";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("Please select an existing Element", GetJsonNodeValue.nodeAsString(content, "message"));
    }


    @DisplayName("isSiblingPerson - Not Siblings")
    @Test
    void isSiblingPersonNotSiblings() throws Exception {
        //Arrange
        final String uri = "/persons/nuno@family.com/siblings/jose@family.com";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("{\"isSibling\":\"false\"}", content);
    }
}