package project.controllers.web.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.dto.AddAccountToGroupRequestInfoDTO;
import project.model.account.Account;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;
import project.utils.GetJsonNodeValue;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AddAccountToGroupRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    AccountRepository accountRepository;

    @DisplayName("addAccountToGroup - Happy Path")
    @Test
    public void addAccountToGroupHappyPath() throws Exception {
        //Arrange
        final String uri = "/groups/BasketFriends/accounts";

        //person
        Date p1BirthDate = new Date(LocalDateTime.of(1982, 03, 14, 0, 0));
        Person p1 = new Person(new Name("Ruben Mendes"), new Address("Porto"), p1BirthDate, new Email("ruben.mendes@gmail.com"), null, null);
        PersonID p1ID = p1.getPersonID();

        personRepository.save(p1);

        //Group repository with group
        Description groupDescription = new Description("BasketFriends");
        Group g1 = new Group(groupDescription, p1ID);

        groupRepository.save(g1);

        final String denomination = "GamesBet";
        final String description = "Bets";
        final String responsibleEmail = "ruben.mendes@gmail.com";

        AddAccountToGroupRequestInfoDTO accountInfoInDTO = new AddAccountToGroupRequestInfoDTO(denomination, description, responsibleEmail);

        //Act
        String entryJson = super.mapToJson(accountInfoInDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("GamesBet", GetJsonNodeValue.nodeAsString(content, "denomination"));
        assertEquals("Bets", GetJsonNodeValue.nodeAsString(content, "description"));
        assertEquals("BasketFriends", GetJsonNodeValue.nodeAsString(content, "ownerID"));
    }

    @DisplayName("addAccountToGroup - Person is not a GroupResponsible")
    @Test
    public void addAccountGroupNotResponsible() throws Exception {
        //Arrange
        final String uri = "/groups/BasketFriends/accounts";

        //person
        Date p1BirthDate = new Date(LocalDateTime.of(1982, 03, 14, 0, 0));
        Person p1 = new Person(new Name("Ruben Mendes"), new Address("Porto"), p1BirthDate, new Email("ruben.mendes@gmail.com"), null, null);
        PersonID p1ID = p1.getPersonID();

        personRepository.save(p1);

        //Group repository with group
        Description groupDescription = new Description("BasketFriends");
        Group g1 = new Group(groupDescription, p1ID);

        groupRepository.save(g1);

        final String denomination = "Groceries Account";
        final String description = "Everything Juicy";
        final String responsibleEmail = "maria@family.com";

        AddAccountToGroupRequestInfoDTO accountInfoInDTO = new AddAccountToGroupRequestInfoDTO(denomination, description, responsibleEmail);

        //Act
        String entryJson = super.mapToJson(accountInfoInDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("Please select an existing Responsible", GetJsonNodeValue.nodeAsString(content, "message"));
        assertEquals("UNPROCESSABLE_ENTITY", GetJsonNodeValue.nodeAsString(content, "status"));
    }

    @DisplayName("addAccountToGroup - GroupID not found")
    @Test
    public void addAccountGroupIDNotFound() throws Exception {
        //Arrange
        final String uri = "/groups/g1/accounts";

        final String denomination = "Groceries Account";
        final String description = "Everything Juicy";
        final String groupDescription = "Non existing Group";
        final String responsibleEmail = "jose@family.com";

        AddAccountToGroupRequestInfoDTO accountInfoInDTO = new AddAccountToGroupRequestInfoDTO(denomination, description, responsibleEmail);
        //Act
        String entryJson = super.mapToJson(accountInfoInDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("Please select an existing GroupID", GetJsonNodeValue.nodeAsString(content, "message"));
        assertEquals("UNPROCESSABLE_ENTITY", GetJsonNodeValue.nodeAsString(content, "status"));
    }

    @DisplayName("addAccountToGroup - That account is already in the repository")
    @Test
    public void addAccountAccountAlreadyInRepository() throws Exception {
        //Arrange
        final String uri = "/groups/g1/accounts";

        Name p1Name = new Name("Ruben Mendes");
        Address p1BirthPlace = new Address("Porto");
        Date p1BirthDate = new Date(LocalDateTime.of(1982, 03, 14, 0, 0));
        Email p1Email = new Email("ruben.mendes@gmail.com");
        Person p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        PersonID p1ID = p1.getPersonID();

        personRepository.save(p1);

        //Group repository with group
        Description groupDescription = new Description("Volleyball Friends");
        Group g1 = new Group(groupDescription, p1ID);

        groupRepository.save(g1);

        //Account in repository
        Account savingsAccount = new Account(new Denomination("Low Savings Account"), new Description("Everything Savings"), g1.getID());

        final String denomination = "Low Savings Account";
        final String description = "Everything Savings";
        final String responsibleEmail = "ruben.mendes@gmail.com";

        AddAccountToGroupRequestInfoDTO accountInfoInDTO = new AddAccountToGroupRequestInfoDTO(denomination, description, responsibleEmail);

        //Act
        String entryJson = super.mapToJson(accountInfoInDTO);

        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("Please select an existing GroupID", GetJsonNodeValue.nodeAsString(content, "message"));
        assertEquals("UNPROCESSABLE_ENTITY", GetJsonNodeValue.nodeAsString(content, "status"));
    }
}