package project.controllers.web.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.dto.AddGroupRequestInfoDTO;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;
import project.utils.GetJsonNodeValue;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddGroupRestControllerIntegrationTest extends AbstractTest {

    @Autowired
    GroupRepository groupRepository;

    @DisplayName("As a user, I want to create a group, becoming a group administrator -add Group - Happy path ")
    @Test
    void addGroupHappyPath() throws Exception {
        //Arrange

        final String uri = "/groups";

        final String groupDescription = "test group";
        final String responsibleEmail = "maria@gmail.com";

        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO(groupDescription, responsibleEmail);

        //Act
        String entryJson = super.mapToJson(addGroupRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();

        assertEquals(201, status);

        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("test group", GetJsonNodeValue.nodeAsString(content, "groupDescription"));
    }


    @DisplayName("/groups - Assert that a specific exception is thrown when the group already exists")
    @Test
    void addGroup_ExistingGroup() throws Exception {
        //Arrange

        Description description = new Description("Group2");
        Email email = new Email("maria@gmail.com");
        PersonID responsible = new PersonID(email);

        Group group2 = new Group(description, responsible);

        groupRepository.save(group2);

        final String uri = "/groups";

        final String groupDescription = "Group2";
        final String responsibleEmail = "maria@gmail.com";

        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO(groupDescription, responsibleEmail);
        //Act

        String entryJson = super.mapToJson(addGroupRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("Please select an nonexistent Group", GetJsonNodeValue.nodeAsString(content, "message"));
        assertEquals("UNPROCESSABLE_ENTITY", GetJsonNodeValue.nodeAsString(content, "status"));
    }
}
