package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AccountDTO;
import project.dto.AddAccountToGroupRequestDTO;
import project.dto.AddAccountToGroupRequestInfoDTO;
import project.exceptions.AlreadyExistsException;
import project.model.account.Account;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;
import project.services.AddAccountToGroupService;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class AddAccountToGroupRestControllerTest {
    Denomination denomination;
    Description description;
    Description groupDescription;
    PersonID responsibleEmail;
    Group group;
    Person p1;
    LocalDateTime birthDate;

    @Autowired
    private AddAccountToGroupRestController addAccountToGroupRestController;
    @Autowired
    private AddAccountToGroupService addAccountToGroupMockService;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private PersonRepository personRepository;

    @DisplayName("Happy Path")
    @Test
    void addAccountToGroupHappyPath() {
        //Arrange
        birthDate = LocalDateTime.of(1982, Month.JANUARY, 01, 0, 0, 0);
        p1 = new Person(new Name("Rui"), new Address("Porto"), new Date(birthDate), new Email("ruisemedo@gmail.com"), null, null);
        personRepository.save(p1);

        description = new Description("CarShopping");
        denomination = new Denomination("Cars");
        groupDescription = new Description("CarLovers");
        responsibleEmail = new PersonID(new Email("ruisemedo@gmail.com"));
        group = new Group(groupDescription, responsibleEmail);
        groupRepository.save(group);

        AccountDTO expected = new AccountDTO();
        expected.setDescription(description);
        expected.setAccountID(new AccountID(denomination, responsibleEmail));

        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = new AddAccountToGroupRequestDTO(denomination, description, responsibleEmail, group.getID());
        Mockito.when(addAccountToGroupMockService.addAccountToGroup(addAccountToGroupRequestDTO)).thenReturn(expected);

        //Act
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO = new AddAccountToGroupRequestInfoDTO("Cars", "CarShopping", "ruisemedo@gmail.com");
        ResponseEntity<Object> result = addAccountToGroupRestController.addAccountToGroup("CarLovers", addAccountToGroupRequestInfoDTO);

        //Asset
        assertEquals(201, result.getStatusCodeValue());
    }

    @DisplayName("addAccountToGroup - Account Already Exist")
    @Test
    void addAccountToGroupAlreadyExists() {
        //Arrange
        final String denominationR = "Cars";
        final String descriptionR = "CarShopping";
        final String responsibleEmail = "rui@gmail.com";

        birthDate = LocalDateTime.of(1982, Month.JANUARY, 01, 0, 0, 0);
        p1 = new Person(new Name("Rui"), new Address("Porto"), new Date(birthDate), new Email("rui@gmail.com"), null, null);
        personRepository.save(p1);

        denomination = new Denomination("Cars");
        description = new Description("CarShopping");
        PersonID emailID = new PersonID(new Email("rui@gmail.com"));
        groupDescription = new Description("CarLovers");
        group = new Group(groupDescription, emailID);
        groupRepository.save(group);

        Account account = new Account(denomination, description, emailID);
        accountRepository.save(account);

        AccountDTO expected = new AccountDTO();
        expected.setDescription(description);
        expected.setAccountID(new AccountID(denomination, emailID));

        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = new AddAccountToGroupRequestDTO(denomination, description, emailID, group.getID());
        Mockito.when(addAccountToGroupMockService.addAccountToGroup(addAccountToGroupRequestDTO)).thenThrow(new AlreadyExistsException(""));

        //Act

        //Assert
        AddAccountToGroupRequestInfoDTO addAccountToGroupRequestInfoDTO = new AddAccountToGroupRequestInfoDTO(denominationR, descriptionR, responsibleEmail);
        assertThrows(AlreadyExistsException.class, () -> {
            ResponseEntity<Object> result = addAccountToGroupRestController.addAccountToGroup(group.getID().toString(), addAccountToGroupRequestInfoDTO);
        });
    }


}

