package project.controllers.web.persistence;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.AbstractTest;
import project.dto.AddAccountToPersonRequestInfoDTO;
import project.model.account.Account;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.PersonRepository;
import project.repositories.jpa.AccountJPARepository;
import project.repositories.jpa.PersonJPARepository;
import project.utils.GetJsonNodeValue;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddAccountToPersonRestControllerInt extends AbstractTest {
    PersonID personId;
    Person person;
    Email email;
    Person mother;
    Person father;
    LocalDateTime personBirthDate;
    LocalDateTime fatherBirthDate;
    LocalDateTime motherBirthDate;


    @Autowired
    PersonRepository personRepository;
    @Autowired
    PersonJPARepository personJPARepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    AccountJPARepository accountJPARepository;

    @BeforeEach
    void setUpTests() {
        //Person1
        personBirthDate = LocalDateTime.of(1982, Month.JANUARY, 1, 0, 0, 0);
        motherBirthDate = LocalDateTime.of(1952, Month.DECEMBER, 15, 0, 0, 0);
        fatherBirthDate = LocalDateTime.of(1951, Month.JANUARY, 10, 0, 0, 0);

        email = new Email("mario@family.com");
        personId = new PersonID(email);

        mother = new Person(new Name("Joana"), new Address("Porto"), new Date(motherBirthDate), new Email("joana@family.com"), null, null);
        father = new Person(new Name("Pedro"), new Address("Porto"), new Date(fatherBirthDate), new Email("pedro@family.com"), null, null);
        person = new Person(new Name("Mário"), new Address("Porto"), new Date(personBirthDate), email, father.getPersonID(), mother.getPersonID());

        personRepository.save(person);
    }

    @DisplayName("addAccountToPerson - Happy path ")
    @Test
    void addAccountToPersonHappyPath() throws Exception {
        //Arrange
        final String uri = "/persons/mario@family.com/accounts";
        final String accountDenomination = "Market Account";
        final String accountDescription = "Fruits";

        assertTrue(personRepository.findById(personId).isPresent());
        assertFalse(accountRepository.findById(new AccountID(new Denomination(accountDenomination), personId)).isPresent());

        //New Account
        Account newAccount = new Account(new Denomination(accountDenomination), new Description(accountDescription), personId);

        AddAccountToPersonRequestInfoDTO infoDTO = new AddAccountToPersonRequestInfoDTO(newAccount.getAccountID().getDenomination().getDenomination(), newAccount.getDescription().getDescription());

        //Act
        String entryJson = super.mapToJson(infoDTO);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("Market Account", GetJsonNodeValue.nodeAsString(content, "denomination"));
        assertEquals("Fruits", GetJsonNodeValue.nodeAsString(content, "description"));

        //Assert persistence
        assertTrue(accountRepository.findById(new AccountID(new Denomination("Market Account"), personId)).isPresent());

        //Delete saved entities
        AccountID accountID = new AccountID(new Denomination("Market Account"), personId);
        accountJPARepository.deleteById(accountID);
        assertFalse(accountJPARepository.findById(accountID).isPresent());
    }

}
