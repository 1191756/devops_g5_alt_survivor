package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.IsSiblingRequestDTO;
import project.exceptions.NotFoundException;
import project.model.person.Person;
import project.model.shared.Address;
import project.model.shared.Date;
import project.model.shared.Email;
import project.model.shared.Name;
import project.services.IsSiblingPersonService;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class IsSiblingPersonRestControllerTest {
    @Autowired
    private IsSiblingPersonService isSiblingPersonMockService;
    @Autowired
    private IsSiblingPersonRestController isSiblingPersonRestController;

    @DisplayName("isSiblingPerson - Happy Path")
    @Test
    public void isSiblingPersonHappyPath() {
        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("mariagarrida@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("josecalame@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisioborges@family.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nunoagostini@family.com");
        Person sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());


        IsSiblingRequestDTO isSiblingRequestDTO = new IsSiblingRequestDTO(son.getPersonID().toString(), sibling.getPersonID().toString());

        Mockito.when(isSiblingPersonMockService.isSiblingPerson(isSiblingRequestDTO)).thenReturn(true);

        //Act
        ResponseEntity<Object> result = isSiblingPersonRestController.isSiblingPerson(son.getPersonID().toString(), sibling.getPersonID().toString());

        assertEquals(200, result.getStatusCodeValue());
    }


    @DisplayName("isSiblingPerson - Not siblings")
    @Test
    public void isSiblingPersonNotSiblings() {

        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("mariagarrida@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("josecalame@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name mariana = new Name("Mariana");
        Email marianaEmail = new Email("mariana@family.com");
        Person mother2 = new Person(mariana, birthAddress, parentsBirthdate, marianaEmail, null, null);
        Name joseCarlos = new Name("José Carlos");
        Email joseCarlosEmail = new Email("josecarlos@family.com");
        Person father2 = new Person(joseCarlos, birthAddress, parentsBirthdate, joseCarlosEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisioborges@family.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nunoagostini@family.com");
        Person otherson = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother2.getPersonID(), father2.getPersonID());


        IsSiblingRequestDTO isSiblingRequestDTO = new IsSiblingRequestDTO(son.getPersonID().toString(), otherson.getPersonID().toString());

        Mockito.when(isSiblingPersonMockService.isSiblingPerson(isSiblingRequestDTO)).thenReturn(false);

        //Act
        ResponseEntity<Object> result = isSiblingPersonRestController.isSiblingPerson(son.getPersonID().toString(), otherson.getPersonID().toString());

        assertEquals(200, result.getStatusCodeValue());
    }


    @DisplayName("isSiblingPerson - No user found(first person)")
    @Test
    public void isSiblingPersonNoUserFoundFirstPerson() {

        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with one son
        Name mariana = new Name("Mariana");
        Email marianaEmail = new Email("mariana@family.com");
        Person mother2 = new Person(mariana, birthAddress, parentsBirthdate, marianaEmail, null, null);
        Name joseCarlos = new Name("José Carlos");
        Email joseCarlosEmail = new Email("josecarlos@family.com");
        Person father2 = new Person(joseCarlos, birthAddress, parentsBirthdate, joseCarlosEmail, null, null);
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nunoagostini@family.com");
        Person otherson = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother2.getPersonID(), father2.getPersonID());


        IsSiblingRequestDTO isSiblingRequestDTO = new IsSiblingRequestDTO("tar@family.com", otherson.getPersonID().toString());

        Mockito.when(isSiblingPersonMockService.isSiblingPerson(isSiblingRequestDTO)).thenThrow(new NotFoundException());

        //Act//Assert
        assertThrows(NotFoundException.class, () -> {
            ResponseEntity<Object> result = isSiblingPersonRestController.isSiblingPerson("tar@family.com", otherson.getPersonID().toString());
        });
    }


    @DisplayName("isSiblingPerson - No user found(second person)")
    @Test
    public void isSiblingPersonNoUserFoundSecondPerson() {

        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("mariagarrida@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("josecalame@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisioborges@family.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());


        IsSiblingRequestDTO isSiblingRequestDTO = new IsSiblingRequestDTO(son.getPersonID().toString(), "nu@family.com");

        Mockito.when(isSiblingPersonMockService.isSiblingPerson(isSiblingRequestDTO)).thenThrow(new NotFoundException());

        //Act//Assert
        assertThrows(NotFoundException.class, () -> {
            ResponseEntity<Object> result = isSiblingPersonRestController.isSiblingPerson(son.getPersonID().toString(), "nu@family.com");
        });
    }

}