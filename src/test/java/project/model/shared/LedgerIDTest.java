package project.model.shared;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.account.Account;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class LedgerIDTest {
    Email email;
    OwnerID ownerID;
    Description groupDescription;
    GroupID gpID;
    LedgerID ledgerID;

    @BeforeEach()
    void Transaction() {
        groupDescription = new Description("Girls Group");
        gpID = new GroupID(groupDescription);
        email = new Email("maria@gmail.com");
        ownerID = new PersonID(email);
    }

    @Test
    @DisplayName("sameValueAs() - False")
    void testEqualsSameAttributesDifObject() {
        //Arrange
        ledgerID = new LedgerID(new OwnerID());
        LedgerID ledgerToCompareID = new LedgerID(new OwnerID());

        //Act
        boolean result = ledgerID.sameValueAs(ledgerToCompareID);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Equals override - Different Object Type")
    @Test
    void testEqualsDifferentObjectType() {
        //Arrange
        AccountID debit = new AccountID(new Denomination("Banc"), gpID);
        Account account = new Account(debit, "Description");
        ledgerID = new LedgerID(new OwnerID());
        //Act
        boolean result = ledgerID.equals(account);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Hash Code Override test")
    @Test
    void hashCodeTest() {
        //Arrange
        ledgerID = new LedgerID(new OwnerID());
        LedgerID ledgerToCompare = new LedgerID(new OwnerID());
        int expectedResult = ledgerID.hashCode();

        //Act
        int result = ledgerToCompare.hashCode();
        //Assert
        assertEquals(expectedResult, result);
    }

    @DisplayName("Equals override - Different id")
    @Test
    void testEqualsDifAmount() {
        //Arrange
        groupDescription = new Description("Girls Group");
        OwnerID ownerIDOther = new GroupID(groupDescription);
        ledgerID = new LedgerID(new OwnerID());
        LedgerID ledgerToCompare = new LedgerID(ownerIDOther);
        //Act
        boolean result = ledgerID.equals(ledgerToCompare);
        //Assert
        assertFalse(result);
    }

    @DisplayName("getID - Happy Path ")
    @Test
    void getAccountIDCredit() {
        //Arrange
        email = new Email("maria@gmail.com");
        ownerID = new PersonID(email);
        ledgerID = new LedgerID(ownerID);

        OwnerID expected = new PersonID(email);
        //Act
        OwnerID result = ledgerID.getId();
        //Assert
        assertEquals(expected, result);
    }
}