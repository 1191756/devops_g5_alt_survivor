package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.EmptyException;

import static org.junit.jupiter.api.Assertions.*;

class EmailTest {
    @DisplayName("create email - Happy Path")
    @Test
    void setIsEmailValidHappyPath() {
        //Arrange//Act//Assert
        new Email("1091064@isep.ipp.pt");
    }

    @DisplayName("create email - Null")
    @Test
    void setIsEmailValidNull() {

        //Arrange//Act//Assert
        assertThrows(NullPointerException.class, () -> {
            new Email(null);
        });
    }

    @DisplayName("create email - Empty")
    @Test
    void setIsEmailValidEmpty() {

        //Arrange//Act//Assert
        assertThrows(EmptyException.class, () -> {
            new Email("");
        });
    }

    @DisplayName("create email - without @")
    @Test
    void setIsEmailValidWithoutArroba() {

        //Arrange//Act//Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Email("testegmail.com");
        });
    }

    @DisplayName("create email - without .com")
    @Test
    void setIsEmailValidWithoutDot() {

        //Arrange//Act//Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Email("teste@gmailcom");
        });
    }

    @DisplayName("getEmail() - Happy Path")
    @Test
    void getEmailHappyPath() {
        //Arrange
        Email personEmail = new Email("user@finances.com");
        String expectedResult = "user@finances.com";

        // Act
        String result = personEmail.getEmail();

        // Assert
        assertEquals(result, expectedResult);
    }

    @DisplayName("create email - sameValueAs")
    @Test
    void setIsEmailValidSameValueAs() {
        //Arrange
        Email emailOne = new Email("1234m@email.com");
        Email emailTwo = new Email("1234m@email.com");

        //Act
        boolean result = emailOne.sameValueAs(emailTwo);

        //Assert
        assertTrue(result);
    }

    @DisplayName("create email - sameValueAsFalse")
    @Test
    void setIsEmailValidSameValueAsFalse() {
        //Arrange
        Email emailOne = new Email("1234m@email.com");
        Email emailTwo = new Email("4321m@email.com");

        //Act
        boolean result = emailOne.sameValueAs(emailTwo);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create email - sameValueAsNull")
    @Test
    void setIsEmailValidSameValueAsNull() {
        //Arrange
        Email email = new Email("1234m@email.com");

        //Act
        boolean result = email.sameValueAs(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("email - Override Equals Different Class")
    @Test
    void setIsDenominationValidOverrideEqualsDifferentClass() {
        //Arrange
        Email email = new Email("1234m@email.com");
        Denomination denomination = new Denomination("Groceries");

        //Act
        boolean result = email.equals(denomination);

        //Assert
        assertFalse(result);
    }

}