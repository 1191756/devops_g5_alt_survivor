package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.EmptyException;

import static org.junit.jupiter.api.Assertions.*;

class DescriptionTest {
    @DisplayName("Create description - Happy Path")
    @Test
    void setIsDescriptionValidHappyPath() {
        //Arrange//Act//Assert
        new Description("Dia do Pai");
    }

    @DisplayName("create description - Null")
    @Test
    void setIsDescriptionValidNull() {
        //Arrange//Act//Assert
        assertThrows(NullPointerException.class, () -> new Description(null));
    }

    @DisplayName("create description - Empty")
    @Test
    void setIsDescriptionValidEmpty() {
        //Arrange//Act//Assert
        assertThrows(EmptyException.class, () -> new Description(" "));
    }

    @DisplayName("getDescription() - Happy Path")
    @Test
    void getDescriptionValidHappyPath() {
        //Arrange
        Description testDescription = new Description("Dia do Pai");

        String expected = "Dia do Pai";

        //Act
        String result = testDescription.getDescription();

        // Assert
        assertEquals(expected, result);
    }

    @DisplayName("equals() - Null Object to compare")
    @Test
    void equalsNullObjectCompare() {
        //Arrange
        Description testDescription = new Description("Dia do Pai");
        Description nullDescription = null;

        //Act
        boolean result = testDescription.equals(nullDescription);

        // Assert
        assertFalse(result);
    }

    @DisplayName("equals() - Object to compare is from different class")
    @Test
    void equalsDifferentClassObjectCompare() {
        //Arrange
        Description testDescription = new Description("Dia do Pai");
        Name compareName = new Name("Nuno");

        //Act
        boolean result = testDescription.equals(compareName);

        // Assert
        assertFalse(result);
    }

    @DisplayName("toString() - Happy Path")
    @Test
    void toStringHappyPath() {
        //Arrange
        Description testDescription = new Description("Dia do Pai");

        String expected = "Dia do Pai";

        //Act
        String result = testDescription.toString();

        // Assert
        assertEquals(expected, result);
    }

    @DisplayName("sameValueAs() - Null Object to compare")
    @Test
    void sameValueAsNullObjectCompare() {
        //Arrange
        Description testDescription = new Description("Dia do Pai");
        Description nullDescription = null;

        //Act
        boolean result = testDescription.sameValueAs(nullDescription);

        // Assert
        assertFalse(result);
    }

    @DisplayName("sameValueAs() - Same description string ")
    @Test
    void sameValueAsSameDescriptionString() {
        //Arrange
        Description testDescription = new Description("Dia do Pai");
        Description compareDescription = new Description("Dia do Pai");

        //Act
        boolean result = testDescription.sameValueAs(compareDescription);

        // Assert
        assertTrue(result);
    }

    @DisplayName("sameValueAs() - Different description string ")
    @Test
    void sameValueAsDifferentDescriptionString() {
        //Arrange
        Description testDescription = new Description("Dia do Pai");
        Description compareDescription = new Description("Dia da Mãe");

        //Act
        boolean result = testDescription.sameValueAs(compareDescription);

        // Assert
        assertFalse(result);
    }
}