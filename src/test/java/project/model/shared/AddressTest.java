package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddressTest {

    @DisplayName("create address - Happy Path")
    @Test
    void setIsAddressValidHappyPath() {
        //Arrange//Act//Assert
        new Address("Rua do Paraíso");
    }

    @DisplayName("create address - Null")
    @Test
    void setIsAddressValidNull() {
        //Arrange//Act//Assert
        assertThrows(NullPointerException.class, () -> {
            new Address(null);
        });
    }

    @DisplayName("create address - Empty")
    @Test
    void setIsAddressValidEmpty() {

        //Arrange//Act//Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Address("");
        });
    }

    @DisplayName("getAddress() - Happy Path")
    @Test
    void getAddressHappyPath() {
        //Arrange
        Address testAddress = new Address("Flowers Road");
        String expected = "Flowers Road";

        //Act
        String result = testAddress.getAddress();

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("equals() - Same Address Object")
    @Test
    void getAddressSameObject() {
        //Arrange
        Address testAddress = new Address("Flowers Road");

        //Act
        boolean result = testAddress.equals(testAddress);

        //Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Same Address Attributes")
    @Test
    void getAddressSameAddressAttributes() {
        //Arrange
        Address testAddress = new Address("Flowers Road");
        Address compareAddress = new Address("Flowers Road");

        //Act
        boolean result = testAddress.equals(compareAddress);

        //Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Address Object sent is null")
    @Test
    void getAddressNullAddress() {
        //Arrange
        Address testAddress = new Address("Flowers Road");
        Address nullAddress = null;

        //Act
        boolean result = testAddress.equals(nullAddress);

        //Assert
        assertFalse(result);
    }

    @DisplayName("equals() - Different class object")
    @Test
    void getAddressDifferentClassObject() {
        //Arrange
        Address testAddress = new Address("Flowers Road");
        Name nuno = new Name("Nuno");

        //Act
        boolean result = testAddress.equals(nuno);

        //Assert
        assertFalse(result);
    }

    @DisplayName("sameValueAs() - Same Address values")
    @Test
    void sameValueAsSameAddressValues() {
        //Arrange
        Address testAddress = new Address("Flowers Road");
        Address compareAddress = new Address("Flowers Road");

        //Act
        boolean result = testAddress.sameValueAs(compareAddress);

        //Assert
        assertTrue(result);
    }

    @DisplayName("sameValueAs() - Different Address values")
    @Test
    void sameValueAsDifferentAddressValues() {
        //Arrange
        Address testAddress = new Address("Flowers Road");
        Address compareAddress = new Address("Kings Road");

        //Act
        boolean result = testAddress.sameValueAs(compareAddress);

        //Assert
        assertFalse(result);
    }

    @DisplayName("sameValueAs() - Address to compare is null")
    @Test
    void sameValueAsAddressIsNull() {
        //Arrange
        Address testAddress = new Address("Flowers Road");
        Address compareAddress = null;

        //Act
        boolean result = testAddress.sameValueAs(compareAddress);

        //Assert
        assertFalse(result);
    }

    @DisplayName("hashCode() - Happy Path")
    @Test
    void hashCodeHappyPath() {
        //Arrange
        Address testAddress = new Address("Flowers Road");

        //Act
        int result = testAddress.hashCode();

        //Assert
        assertEquals(322605351, result);
    }
}