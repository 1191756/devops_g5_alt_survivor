package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AddTransactionToGroupRequestDTO;
import project.dto.TransactionDTO;
import project.dto.assemblers.LedgerAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.shared.LedgerID;
import project.model.shared.OwnerID;
import project.model.shared.TransactionDate;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;
import project.utils.ClockUtil;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class AddTransactionToGroupService {

    @Autowired
    PersonRepository personRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    LedgerRepository ledgerRepository;
    @Autowired
    ClockUtil clockUtil;

    /**
     * @param addTransactionToGroupRequestDTO description
     * @return transaction DTO
     * Finds group and person on repository and adds new transaction if person is responsible
     */
    public TransactionDTO addTransactionToGroup(AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO) {
        Optional<Group> groupOptional = groupRepository.findById(addTransactionToGroupRequestDTO.getGroupID());

        if (!groupOptional.isPresent()) {
            throw new NotFoundException("GroupID");
        }

        Group targetGroup = groupOptional.get();

        if (!targetGroup.getCategories().contains(addTransactionToGroupRequestDTO.getCategory())) {
            throw new NotFoundException("Category");
        }

        boolean isMember = !targetGroup.isMemberID(addTransactionToGroupRequestDTO.getPersonID());

        if (isMember) {
            throw new NotFoundException("Member");
        }

        Transaction transaction = new Transaction(addTransactionToGroupRequestDTO.getAmount(),
                addTransactionToGroupRequestDTO.getDescription(),
                new TransactionDate(LocalDateTime.now(clockUtil.clock())),
                addTransactionToGroupRequestDTO.getCategory(),
                addTransactionToGroupRequestDTO.getDebit(),
                addTransactionToGroupRequestDTO.getCredit(),
                addTransactionToGroupRequestDTO.getType()
        );

        Optional<Ledger> ledgerOptional = ledgerRepository.findById(new LedgerID(new OwnerID(addTransactionToGroupRequestDTO.getGroupID().getId())));
        Ledger ledger;

        if (!ledgerOptional.isPresent()) {
            ledger = new Ledger(new LedgerID((new OwnerID(addTransactionToGroupRequestDTO.getGroupID().getId()))));
        } else {
            ledger = ledgerOptional.get();
        }

        //Ledger newLedger= new Ledger(ledger.addTransaction(transaction));

        ledger.addTransaction(transaction);

        Ledger saveLedger = ledgerRepository.save(ledger);


        return LedgerAssembler.mapToDTO(transaction);
    }
}
