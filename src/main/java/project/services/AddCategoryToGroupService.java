package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AddCategoryRequestDTO;
import project.dto.CategoryDTO;
import project.dto.assemblers.CategoryAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.shared.Category;
import project.repositories.GroupRepository;

import java.util.Optional;

@Service
public class AddCategoryToGroupService {

    @Autowired
    private final GroupRepository groupRepository;

    public AddCategoryToGroupService(GroupRepository groupRepo) {
        this.groupRepository = groupRepo;
    }

    /**
     * As responsible of the group, I want to create a category for group.
     *
     * @param addCategoryRequestDTO Data Transfer Object
     * @return categoryDTO
     */
    public CategoryDTO addCategoryToGroup(AddCategoryRequestDTO addCategoryRequestDTO) {

        Optional<Group> groupOptional = groupRepository.findById(addCategoryRequestDTO.getGroupDescription());
        if (!groupOptional.isPresent()) {
            throw new NotFoundException("GroupID");
        }

        Group targetGroup = groupOptional.get();
        if (!targetGroup.isResponsible(addCategoryRequestDTO.getResponsibleEmail())) {
            throw new NotFoundException("Responsible");
        }

        Category category = new Category(addCategoryRequestDTO.getDesignation());
        CategoryDTO categoryDTO = CategoryAssembler.mapToDTO(category);
        targetGroup.addCategory(category);
        groupRepository.save(targetGroup);
        return categoryDTO;
    }
}