package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AddMemberRequestDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.GroupAssembler;
import project.exceptions.AlreadyExistsException;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.repositories.GroupRepository;

import java.util.Optional;
@Service
public class AddMemberToGroupService {
    @Autowired
    GroupRepository groupRepository;
    /**
     * Finds group on groupRepository and adds person on that group
     *
     * @param addMemberRequestDTO description
     * @return GroupDTO
     */
    public GroupDTO addMember(AddMemberRequestDTO addMemberRequestDTO) {
        Optional<Group> groupOptional = groupRepository.findById(addMemberRequestDTO.getGroupID());
        if (!groupOptional.isPresent()) {
            throw new NotFoundException("GroupID");
        }

        Group group = groupOptional.get();
        if (group.isMemberID(addMemberRequestDTO.getPersonID())) {
            throw new AlreadyExistsException("Member");
        }

        group.addMember(addMemberRequestDTO.getPersonID());
        Group savedGroup = groupRepository.save(group);
        return GroupAssembler.mapToDTO(savedGroup);
    }
}



