package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.GroupDTO;
import project.dto.assemblers.GroupAssembler;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class GetFamiliesService {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    PersonRepository personRepository;

    public Set<GroupDTO> getFamilies() {
        Set<GroupDTO> families = new HashSet<>();
        for (Group group : groupRepository.findAll()) {
            Set<PersonID> members = group.getMembers();
            if (members.size() >= 3) {
                for (PersonID memberID : members) {
                    Optional<Person> personOptional = personRepository.findById(memberID);
                    if (personOptional.isPresent()) {
                        Person member = personOptional.get();
                        if (group.isMemberID(member.getFather()) && group.isMemberID(member.getMother())) {
                            families.add(GroupAssembler.mapToDTO(group));
                        }
                    }
                }
            }
        }
        return families;
    }
}