package project.repositories.jpa;

import org.springframework.data.repository.CrudRepository;
import project.datamodel.AccountData;
import project.model.shared.AccountID;
import project.model.shared.Denomination;
import project.model.shared.OwnerID;

public interface AccountJPARepository extends CrudRepository<AccountData, AccountID> {
    Iterable<AccountData> findAllByAccountID_OwnerID(OwnerID ownerId);

    AccountData findByAccountID_DenominationAndAccountID_OwnerID(Denomination denomination, OwnerID ownerId);
}
