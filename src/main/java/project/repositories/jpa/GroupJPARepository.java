package project.repositories.jpa;

import org.springframework.data.repository.CrudRepository;
import project.datamodel.GroupData;
import project.datamodel.GroupIDData;

public interface GroupJPARepository extends CrudRepository<GroupData, GroupIDData> {

}
