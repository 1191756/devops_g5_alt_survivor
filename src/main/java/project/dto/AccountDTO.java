package project.dto;

import project.model.shared.AccountID;
import project.model.shared.Description;

public class AccountDTO {
    private Description description;
    private AccountID accountID;

    public AccountDTO() {
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public AccountID getAccountID() {
        return accountID;
    }

    public void setAccountID(AccountID accountID) {
        this.accountID = accountID;
    }
}
