package project.dto.assemblers;

import project.dto.TransactionDTO;
import project.model.ledger.Transaction;

public class LedgerAssembler {

    protected LedgerAssembler() {
        throw new AssertionError();
    }

    public static TransactionDTO mapToDTO(Transaction transaction) {
        TransactionDTO transactionDTO = new TransactionDTO();

        transactionDTO.setAmount(transaction.getAmount());
        transactionDTO.setDescription(transaction.getDescription());
        transactionDTO.setDate(transaction.getDate());
        transactionDTO.setCategory(transaction.getCategory());
        transactionDTO.setDebit(transaction.getDebit());
        transactionDTO.setCredit(transaction.getCredit());
        transactionDTO.setType(transaction.getType());

        return transactionDTO;
    }
}
