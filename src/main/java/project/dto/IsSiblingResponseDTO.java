package project.dto;

import java.util.Objects;

public class IsSiblingResponseDTO {
    private String isSibling;

    public IsSiblingResponseDTO(String isSibling) {
        this.isSibling = isSibling;
    }

    public String getIsSibling() {
        return isSibling;
    }

    public void setIsSibling(String isSibling) {
        this.isSibling = isSibling;
    }

    @Override
    public String toString() {
        return isSibling;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IsSiblingResponseDTO)) return false;
        IsSiblingResponseDTO that = (IsSiblingResponseDTO) o;
        return Objects.equals(isSibling, that.isSibling);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isSibling);
    }
}
