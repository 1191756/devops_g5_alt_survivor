package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CategoryResponseDTO extends RepresentationModel<CategoryResponseDTO> {

    private String designation;

    public CategoryResponseDTO(String designation) {
        this.designation = designation;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CategoryResponseDTO that = (CategoryResponseDTO) o;
        return Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation);
    }
}