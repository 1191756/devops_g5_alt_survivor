package project.dto;

import project.model.shared.Designation;

public class CategoryDTO {
    private Designation designation;

    public CategoryDTO() {
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

}
