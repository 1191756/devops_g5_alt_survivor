package project.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import project.model.account.Account;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;

@Profile("!test")
@Component
public class RepositoryLoader implements ApplicationRunner {
    @Autowired
    private final PersonRepository personRepository;
    @Autowired
    private final GroupRepository groupRepository;
    @Autowired
    private final AccountRepository accountRepository;
    @Autowired
    private final LedgerRepository ledgerRepository;

    public RepositoryLoader(PersonRepository personRepository, GroupRepository groupRepository, AccountRepository accountRepository, LedgerRepository ledgerRepository) {
        this.personRepository = personRepository;
        this.groupRepository = groupRepository;
        this.accountRepository = accountRepository;
        this.ledgerRepository = ledgerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisio@family.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nuno@family.com");
        Person sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());
        son.addSibling(sibling.getPersonID());

        //Categories
        Category categoryBananinhas = new Category(new Designation("vananas"));
        Category categoryNanasis = new Category(new Designation("Nanasis"));

        personRepository.save(mother);
        personRepository.save(father);
        personRepository.save(son);
        personRepository.save(sibling);

        // GROUPS
        Description groupDescription = new Description("Family");
        Group familyGroup = new Group(groupDescription, father.getPersonID());
        GroupID familyGroupID = familyGroup.getID();

        Description group2Description = new Description("Friends");
        Group friendsGroup = new Group(group2Description, son.getPersonID());
        GroupID friendsGroupID = friendsGroup.getID();

        familyGroup.addCategory(categoryBananinhas);
        familyGroup.addCategory(categoryNanasis);
        Group savedGroup = groupRepository.save(familyGroup);

        friendsGroup.addCategory(categoryBananinhas);
        friendsGroup.addCategory(categoryNanasis);
        groupRepository.save(friendsGroup);

        Account account1 = new Account(new Denomination("Conta Pessoa"), new Description("Fabulous Group 5"), mother.getPersonID());
        Account account2 = new Account(new Denomination("Conta Grupo 1"), new Description("Fabulous Group 1"), familyGroup.getID());
        Account account3 = new Account(new Denomination("Conta Grupo 2"), new Description("Fabulous Group 2"), familyGroup.getID());
        Account account4 = new Account(new Denomination("Conta Grupo 3"), new Description("Fabulous Group 3"), familyGroup.getID());
        accountRepository.save(account1);
        accountRepository.save(account2);
        accountRepository.save(account3);
        accountRepository.save(account4);

        System.out.println(accountRepository.findById(new AccountID(new Denomination("Conta Pessoa"), mother.getPersonID())));
        System.out.println(accountRepository.findById(new AccountID(new Denomination("Conta Grupo 1"), familyGroup.getID())));

        for (Account account : accountRepository.findAllByAccountID_OwnerID(familyGroup.getID())) {
            System.out.println("Groups:");
            System.out.println(account.toString());
        }

        for (Account account : accountRepository.findAllByAccountID_OwnerID(mother.getPersonID())) {
            System.out.println("Persons:");
            System.out.println(account.toString());
        }

/*        Ledger ledger1 = new Ledger(new LedgerID(savedGroup.getID()));

        System.out.println("LedgerID Group:" + ledger1.getLedgerID().toString());

       Transaction transaction1 = new Transaction(10.0,
                new Description("primeira transacao"),
                new Date(LocalDateTime.now()),
                categoryNanasis,
                account2.getAccountID(),
                account3.getAccountID(),
                CREDIT
                );

        ledger1.addTransaction(transaction1);
        ledgerRepository.save(ledger1);*/

    }
}

