package project.datamodel;

import lombok.NoArgsConstructor;
import project.model.shared.LedgerID;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor

@Entity
@Table(name = "Ledgers")
public class LedgerData implements Serializable {
    private static final long serialVersionUID = 21589603674571L;
    @EmbeddedId
    private LedgerID ledgerID;
    @OneToMany(cascade = CascadeType.ALL)
    public List<TransactionData> transactions;

    public LedgerData(LedgerID ledgerID) {
        this.ledgerID = ledgerID;
        this.transactions = new ArrayList<>();
    }

    public LedgerID getLedgerID() {
        return ledgerID;
    }

    public void setLedgerID(LedgerID ledgerID) {
        this.ledgerID = ledgerID;
    }

    public List<TransactionData> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionData> transactions) {
        this.transactions = transactions;
    }

    public void setTransaction(TransactionData transactionData) {
        this.transactions.add(transactionData);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LedgerData)) return false;
        LedgerData that = (LedgerData) o;
        return Objects.equals(ledgerID, that.ledgerID) &&
                Objects.equals(transactions, that.transactions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ledgerID, transactions);
    }

    @Override
    public String toString() {
        return "LedgerData{" +
                "ledgerID=" + ledgerID +
                ", transactions=" + transactions +
                '}';
    }
}