package project.datamodel;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@IdClass(GroupCategoryIDData.class)
@Table(name = "groups_categories")
public class GroupCategoryData implements Serializable {
    private static final long serialVersionUID = 91989003634571L;
    @Id
    private String id;
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id", nullable = false)
    private GroupData group;

    public GroupCategoryData() {
    }

    public GroupCategoryData(String id, GroupData group) {
        this.id = id;
        this.group = group;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupCategoryData that = (GroupCategoryData) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(group, that.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, group);
    }
}
