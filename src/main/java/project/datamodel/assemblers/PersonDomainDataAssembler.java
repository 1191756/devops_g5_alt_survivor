package project.datamodel.assemblers;

import org.springframework.stereotype.Service;
import project.datamodel.PersonCategoryData;
import project.datamodel.PersonData;
import project.datamodel.SiblingIDData;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;

/**
 * Conversion Class for Person Objects used in database operations
 */
@Service
public class PersonDomainDataAssembler {
    /**
     * Person information is assembled into Data format
     *
     * @param person param description
     * @return instance of PersonData
     */
    public static PersonData toData(Person person) {
        PersonData personData = new PersonData(new PersonID(new Email(person.getPersonID().toString())),
                person.getName().toString(),
                person.getBirthPlace().toString(),
                person.getBirthDate().toString());

        if (person.getMother() != null)
            personData.setMother(person.getMother().toString());

        if (person.getFather() != null)
            personData.setFather(person.getFather().toString());
        if (person.getSiblings().size() > 0)
            for (PersonID id : person.getSiblings()) {
                personData.setSibling(new SiblingIDData(id.toString()));
            }

        if (person.getCategories().size() > 0) {
            for (Category category : person.getCategories()) {
                personData.setCategory(new PersonCategoryData(category.getDesignation().getDesignation(), personData));
            }
        }
        return personData;
    }

    /**
     * PersonData information is assembled into a Person object
     *
     * @param personData param description
     * @return instance of Person
     */
    public static Person toDomain(PersonData personData) {

        String str = personData.getBirthDate();
        String[] dateSplit = str.split("-");
        Date birthdate = new Date(LocalDateTime.of(Integer.parseInt(dateSplit[0]), Integer.parseInt(dateSplit[1]), Integer.parseInt(dateSplit[2]), 0, 0, 0));

        PersonID father = null;
        PersonID mother = null;

        if (personData.getFather() != null) {
            father = new PersonID(new Email(personData.getFather()));
        }

        if (personData.getFather() != null) {
            mother = new PersonID(new Email(personData.getMother()));
        }

        Person person = new Person(new Name(personData.getName()),
                new Address(personData.getBirthPlace()),
                birthdate,
                new Email(personData.getId().toString()),
                mother,
                father
        );

        if (personData.getSiblings().size() > 0) {
            for (SiblingIDData siblingIDData : personData.getSiblings()) {
                person.addSibling(new PersonID(new Email(siblingIDData.getId())));
            }
        }

        if (personData.getCategories().size() > 0) {
            for (PersonCategoryData categoryData : personData.getCategories()) {
                person.addCategory(new Category(new Designation(categoryData.toString())));
            }
        }
        return person;
    }
}

