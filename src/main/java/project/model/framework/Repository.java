package project.model.framework;

import java.util.Optional;

public interface Repository<T, K> {
    <S extends T> S save(S entity);

    /**
     * Gets all entities from the repository.
     *
     * @return all entities from the repository
     */
    Iterable<T> findAll();

    /**
     * Gets the entity with the specified primary key.
     *
     * @param id desc
     * @return the entity with the specified primary key
     */
    Optional<T> findById(K id);
}