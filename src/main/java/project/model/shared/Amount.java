package project.model.shared;

import project.model.framework.ValueObject;

import java.util.Objects;

public class Amount implements ValueObject {

    private double amount;

    public Amount(double amount) {
        setAmountIfValid(amount);
    }

    /**
     * Verifies if provided amount is null or empty and defines the attribute with the value received
     *
     * @param amount value of the amount
     */
    private void setAmountIfValid(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Amount amountToCompare = (Amount) o;
        return this.equals(amountToCompare);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount);

    }

    @Override
    public boolean sameValueAs(Object other) {
        return false;
    }
}

