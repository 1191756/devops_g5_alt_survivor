package project.model.shared;

import project.model.framework.ValueObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Date implements ValueObject<Date> {
    private LocalDateTime date;

    public Date(LocalDateTime date) {
        setIsDateValid(date);
    }

    private void setIsDateValid(LocalDateTime date) {
        if (date != null) {
            this.date = date;
        } else {
            throw new NullPointerException("Date can't be null");
        }
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Date date1 = (Date) o;
        return date.equals(date1.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date);
    }

    @Override
    public boolean sameValueAs(Date other) {
        return other != null && this.date.equals(other.date);
    }

    @Override
    public String toString() {
        return date.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
}
