package project.model.shared;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.NoArgsConstructor;
import project.exceptions.EmptyException;
import project.model.framework.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor

@JsonDeserialize
@Embeddable
public class Description implements ValueObject<Description>, Serializable {
    private static final long serialVersionUID = 16222991423483L;
    private String description;

    public Description(String description) {
        setIsDescriptionValid(description);
    }

    /**
     * Checks if parameters are null or empty and defines the attribute with the value received.
     *
     * @param description text describing object
     */
    private void setIsDescriptionValid(String description) {
        if (description == null) {
            throw new NullPointerException("Description cannot be null");
        }
        if (description.trim().isEmpty()) {
            throw new EmptyException("Description cannot be empty");
        }
        this.description = description.trim();
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Description descriptionToCompare = (Description) o;
        return description.equals(descriptionToCompare.description);
    }

    @Override
    public String toString() {
        return description;
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }

    @Override
    public boolean sameValueAs(Description other) {
        return other != null && this.description.equals(other.description);
    }
}
