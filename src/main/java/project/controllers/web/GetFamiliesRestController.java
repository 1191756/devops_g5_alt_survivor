package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import project.dto.GetFamiliesResponseDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.GetFamiliesResponseAssembler;
import project.services.GetFamiliesService;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
public class GetFamiliesRestController {
    @Autowired
    private GetFamiliesService service;

    @GetMapping("/groups")
    @ResponseBody
    public ResponseEntity<Object> getFamilies(@RequestParam String type) {

        if (type.equals("families")) {
            Set<GroupDTO> familiesDTO = service.getFamilies();

            Set<GetFamiliesResponseDTO> getFamiliesResponseDTO = new HashSet<>();

            for (GroupDTO familyDTO : familiesDTO) {
                GetFamiliesResponseDTO familiesResponseDTO = GetFamiliesResponseAssembler.mapToDTO(familyDTO);
                Link selfLink = linkTo(AddGroupRestController.class).slash("groups").slash(familyDTO.getGroupID().getDescription()).withSelfRel();
                familiesResponseDTO.add(selfLink);
                getFamiliesResponseDTO.add(familiesResponseDTO);
            }
            return new ResponseEntity<>(getFamiliesResponseDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>("[]", HttpStatus.OK);
    }
}
