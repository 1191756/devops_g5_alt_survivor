package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.dto.IsSiblingRequestDTO;
import project.dto.IsSiblingResponseDTO;
import project.dto.assemblers.IsSiblingRequestDTOAssembler;
import project.dto.assemblers.IsSiblingResponseAssembler;
import project.services.IsSiblingPersonService;


@RestController
@RequestMapping
public class IsSiblingPersonRestController {
    @Autowired
    private IsSiblingPersonService service;

    @GetMapping(value = "/persons/{PersonID1}/siblings/{PersonID2}")
    public ResponseEntity<Object> isSiblingPerson(@PathVariable String PersonID1, @PathVariable String PersonID2) {
        IsSiblingRequestDTO isSiblingRequestDTO = IsSiblingRequestDTOAssembler.mapToDTO(PersonID1, PersonID2);

        boolean isSibling = false;

        isSibling = service.isSiblingPerson(isSiblingRequestDTO);

        IsSiblingResponseDTO isSiblingResponseDTO = IsSiblingResponseAssembler.mapToDTO(String.valueOf(isSibling));

        return new ResponseEntity<>(isSiblingResponseDTO, HttpStatus.OK);
    }
}
