package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.AccountDTO;
import project.dto.AccountResponseDTO;
import project.dto.AddAccountToGroupRequestDTO;
import project.dto.AddAccountToGroupRequestInfoDTO;
import project.dto.assemblers.AccountResponseAssembler;
import project.dto.assemblers.AddAccountToGroupRequestAssembler;
import project.services.AddAccountToGroupService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping
public class AddAccountToGroupRestController {
    @Autowired
    AddAccountToGroupService service;

    @PostMapping("/groups/{groupId}/accounts")
    public ResponseEntity<Object> addAccountToGroup(@PathVariable String groupId, @RequestBody AddAccountToGroupRequestInfoDTO info) {
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO(info.getDenomination(),
                info.getDescription(),
                info.getResponsibleEmail(),
                groupId);

        AccountDTO accountDTO = service.addAccountToGroup(addAccountToGroupRequestDTO);

        AccountResponseDTO accountResponseDTO = AccountResponseAssembler.mapToDTO(accountDTO);

        Link selfLink = linkTo(AddAccountToGroupRestController.class).slash("groups").slash(groupId).slash("accounts").slash(accountResponseDTO.getDescription()).withSelfRel();
        accountResponseDTO.add(selfLink);

        return new ResponseEntity<>(accountResponseDTO, HttpStatus.CREATED);
    }
}
