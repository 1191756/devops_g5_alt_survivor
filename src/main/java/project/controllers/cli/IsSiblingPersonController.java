package project.controllers.cli;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.dto.IsSiblingRequestDTO;
import project.dto.assemblers.IsSiblingRequestDTOAssembler;
import project.services.IsSiblingPersonService;

@Component
public class IsSiblingPersonController {
    @Autowired
    private IsSiblingPersonService service;

    public boolean isSibling(String aPersonId, String aSecondPersonId) {
        IsSiblingRequestDTO isSiblingRequestDTO = IsSiblingRequestDTOAssembler.mapToDTO(aPersonId, aSecondPersonId);

        return service.isSiblingPerson(isSiblingRequestDTO);
    }
}
