# Final Project Personal Finance - Group 5 
**In Bitbucket, we created a fork:**
https://bitbucket.org/BuckRogers_1/devops_g5/src/master/
___________________________________
### Work Division 
We all worked in whole project together
___________________________________
### Objectives:
The objective of this work is to create:

##### - VM with Jenkins, Ansible and Docker

*Build Stages of the Pipeline for Jenkins:*
- Assemble the application; 
- Generate and publish javadoc in Jenkins; 
- Execute tests (unit and integration) 
- Publish its results in Jenkins (including code coverage)
- Publish the distributable artifacts in Jenkins (e.g., jar ﬁles) 

*Build Stages of the Pipeline for Docker:*
- Create 2 docker images (app + db) and publish them in docker hub 

*Build Stages of the Pipeline for Ansible:*
- Deploy and conﬁgure the application and its database - we used PostgreSQL

##### - VM to install and conﬁgure the Personal Finance web application 

##### - VM to install and conﬁgure the database of the application
__________________________________
### To achieve all the goals:

To complete this assigment we previous had: 

Version Control: Git 

Repository: Bitbucket 

IDE: IntelliJ IDEA 

*Below we describe all the tasks, files and folders that we need to perform in order to achieve all the proposed objectives.*
____________________
#### SSH folder:
In command line we run: 

	ssh-keygen

This will generate two files, that we added to the shared folder in our repository, so the vagrant can copie the file to inside the folder .ssh in the virtual machine. 
We know that this isn't the best practice, in a security level, because we put available in the repository the private key.

The following files have been added to this folder: 

***- id_rsa*** - private key

***- id_rsa.pub*** - public key
____________________
#### Postgres_Vagrantfile

Postgres is the database we use, so we configure this file so Vagrant could install and use it:

    # See: https://manski.net/2016/09/vagrant-multi-machine-tutorial/
    # for information about machine names on private network
    Vagrant.configure("2") do |config|
    
      config.vm.box = "envimation/ubuntu-xenial"
    
      # This provision is common for both VMs
      config.vm.provision "shell", inline: <<-SHELL
        sudo apt-get update -y
        sudo apt-get install iputils-ping -y
        sudo apt-get install -y avahi-daemon libnss-mdns
        sudo apt-get install -y unzip
        sudo apt-get install openjdk-8-jdk-headless -y
        # ifconfig
      SHELL
    
      config.vm.synced_folder ".", "/shared"
      config.ssh.username = "vagrant"
      config.ssh.password = "vagrant"
    
      config.vm.define "postgresql" do |postgresql|
      #============ 
    # Criação de VM com instalação do Postgresql 
    
        postgresql.vm.provision "shell", privileged: false, inline: <<-SHELL
    
        wget -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
        sudo apt-get install postgresql postgresql-contrib
        sudo apt-get remove postgresql-9.1 postgresql-contrib-9.1 postgresql-client-9.1
        sudo apt-get install -y postgresql postgresql-contrib
        sudo sed -i "s/port = 5433/port = 5432/" /etc/postgresql/9.5/main/postgresql.conf
    
    SHELL
      
      # PostgreSQL Server port forwarding
      postgresql.vm.network "forwarded_port", guest: 5432, host: 15432
      postgresql.vm.network "private_network", ip: "192.168.33.12"
    end
    
      #============
      # Configurations specific to the webserver VM
      config.vm.define "web" do |web|
    
        web.vm.box = "envimation/ubuntu-xenial"
        web.vm.hostname = "web"
        web.vm.network "private_network", ip: "192.168.33.10"
    
        # We set more ram memmory for this VM
        web.vm.provider "virtualbox" do |v|
          v.memory = 1024
        end
    
        # We want to access tomcat from the host using port 8080
        web.vm.network "forwarded_port", guest: 8080, host: 8080
    
       web.vm.provision "shell", privileged: false, inline: <<-SHELL
          sudo apt-get install -y ssh
          sudo apt-get install git -y
          sudo apt-get install nodejs -y
          sudo apt-get install npm -y
          sudo ln -s /usr/bin/nodejs /usr/bin/node
          sudo apt install tomcat8 -y
          sudo apt install tomcat8-admin -y
          # If you want to access Tomcat admin web page do the following:
          # Edit /etc/tomcat8/tomcat-users.xml
          # uncomment tomcat-users and add manager-gui to tomcat user
    
          #install maven
          sudo apt-get install maven -y
    
          #Clean .ssh
        
        rm -rf .ssh
        mkdir .ssh
        sudo chmod 700 /home/vagrant/.ssh
        sudo chown vagrant:vagrant /home/vagrant/.ssh
    
          #Get private key
        cat /shared/ssh/id_rsa >> /home/vagrant/.ssh/id_rsa
        sudo chmod 700 /home/vagrant/.ssh/id_rsa
        sudo chown vagrant:vagrant /home/vagrant/.ssh/id_rsa
        cat /shared/ssh/id_rsa.pub >> /home/vagrant/.ssh/id_rsa.pub
        sudo chmod 700 /home/vagrant/.ssh/id_rsa
        sudo chown vagrant:vagrant /home/vagrant/.ssh/id_rsa.pub
    #       cp /shared/ssh/id_rsa /home/vagrant/.ssh/
    #       cp /shared/ssh/id_rsa.pub /home/vagrant/.ssh/
    #       cp /shared/ssh/authorized_keys /home/vagrant/.ssh/
             cp /shared/ssh/config /home/vagrant/.ssh/
    #	sudo chmod 700 /home/vagrant/.ssh/id_rsa
    #	sudo chmod 700 /home/vagrant/.ssh/id_rsa.pub
    #	sudo chmod 600 /home/vagrant/.ssh/authorized_keys
        sudo chmod 700 /home/vagrant/.ssh/config
        sudo chown vagrant:vagrant /home/vagrant/.ssh/config
    
        ssh-keyscan -t rsa bitbucket.org >> /home/vagrant/.ssh/known_hosts
        sudo chmod 700 /home/vagrant/.ssh/known_hosts
        sudo chown vagrant:vagrant /home/vagrant/.ssh/known_hosts
    
        cat /shared/ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
        sudo chmod 600 /home/vagrant/.ssh/authorized_keys
        sudo chown vagrant:vagrant /home/vagrant/.ssh/authorized_keys
    
        eval `ssh-agent`
        ssh-add /home/vagrant/.ssh/id_rsa
        ssh-copy-id -i /home/vagrant/.ssh/id_rsa BuckRogers_1@bitbucket.org
    
          # Clean repository
    #	sudo rm -rf devops_g5 
        
          # Clone repository
          git clone git@bitbucket.org:BuckRogers_1/devops_g5.git
          cd devops_g5
        mvn compile war:war
    #      mvn spring-boot:run
    
          # To deploy the war file to tomcat8 do the following command:
          sudo cp /home/vagrant/devops_g5/target/training-1.0-SNAPSHOT.war /var/lib/tomcat8/webapps
        SHELL
    
      end
    
    end
    postgresql.vm.box = "envimation/ubuntu-xenial"

____________________
#### .mvn/wrapper folder

This folder was created to be used when called by the vagrant to ensure that the Maven's versions used are correct and the same on all machines.
This makes it possible to use Maven on a computer that does not have it installed.

The following files have been added to this folder:

***- MavenWrapperDownloader.java***

***- maven-wrapper.properties***

____________________
And this are the Executable file, for all OS types: 

***- mvnw file***

***- mvnw.cmd file***
____________________
#### Vagrant SetUp folder

This folder was automatically created  when we run on command line:

		 vagrant up

The following file was added to this folder:

***bootstrap. sh***
___________________________________________
#### Vagrantfile

This file was created to configure how the VMs are created.

General settings applied to all VMs:

	# See: https://manski.net/2016/09/vagrant-multi-machine-tutorial/
	# for information about machine names on private network
	Vagrant.configure("2") do |config| 

	  config.vm.box = "envimation/ubuntu-xenial" - OS installed in VMs
      config.vm.synced_folder ".", "/shared", mount_options: ["dmode=775,fmode=600"]
    
      config.ssh.username = "vagrant"
      config.ssh.password = "vagrant"

Open a shell and insert instructions of commands automatically. 
In this case, applies to each VM's Configuration provisioning step:

     # This provision is common for both VMs
      config.vm.provision "shell", inline: <<-SHELL
        sudo apt-get update -y
        sudo apt-get install iputils-ping -y
        sudo apt-get install python3 --yes
        sudo apt-get install -y avahi-daemon libnss-mdns
        sudo apt-get install -y unzip
        # ifconfig
      SHELL

This refer to the creation of two VMs, one install and configure an empty database with Postgres (db2) and another (db) with H2 so we can run the tests.

     #============
      # Configurations specific to the database VM
        config.vm.define "db2" do |db|
        db.vm.box = "envimation/ubuntu-xenial"
        db.vm.hostname = "db2"
        db.vm.network "private_network", ip: "192.168.33.10"
      end
    
      #============
      # Configurations specific to the database VM
      config.vm.define "db" do |db|
        db.vm.box = "envimation/ubuntu-xenial"
        db.vm.hostname = "db"
        db.vm.network "private_network", ip: "192.168.33.11"
    
        # We want to access H2 console from the host using port 8082
        # We want to connet to the H2 server using port 9092
        db.vm.network "forwarded_port", guest: 8082, host: 8082
        db.vm.network "forwarded_port", guest: 9092, host: 9092
    
        # We need to download H2
        db.vm.provision "shell", inline: <<-SHELL
          wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
        SHELL
    
        # The following provision shell will run ALWAYS so that we can execute the H2 server process
        # This could be done in a different way, for instance, setiing H2 as as service, like in the following link:
        # How to setup java as a service in ubuntu: http://www.jcgonzalez.com/ubuntu-16-java-service-wrapper-example
        #
        # To connect to H2 use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
        db.vm.provision "shell", :run => 'always', inline: <<-SHELL
          java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
        SHELL
      end

This refer to the creation of a VM, that install and configure Jenkins, Ansible and Docker:

	  #============
        config.vm.define "jenkins" do |jenkins|
          jenkins.vm.box = "envimation/ubuntu-xenial"
          jenkins.vm.hostname = "jenkins"
          jenkins.vm.network "private_network", ip: "192.168.33.13"
          jenkins.vm.network "forwarded_port", guest: 8080, host: 8085
      
          jenkins.vm.provider "virtualbox" do |vb|
            vb.gui = false
            vb.cpus = 2
            vb.memory = "4096"
          end
      
          config.ssh.username = "vagrant"
          config.ssh.password = "vagrant"
      
          jenkins.vm.provision "shell", inline: <<-SHELL
            sudo apt-get install -y --no-install-recommends apt-utils
            sudo apt-get install software-properties-common --yes
            sudo apt-get install openjdk-8-jdk-headless -y
            sudo apt-add-repository --yes --u ppa:ansible/ansible
            sudo apt-get install ansible --yes
            sudo apt-get install -y apt-transport-https
            wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
            sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
            sudo apt-get update
            sudo apt-get install -y git
            sudo apt-get update
            sudo apt-get install -y\
            >     apt-transport-https \
            >     ca-certificates \
            >     curl \
            >     gnupg-agent \
            >     software-properties-common
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
            sudo add-apt-repository \
            >    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
            >    $(lsb_release -cs) \
            >    stable"
            sudo apt-get update
            sudo apt-get install docker-ce docker-ce-cli containerd.io
            sudo apt-get install -y jenkins
            sudo service jenkins start
            sleep 1m
            JENKINSPWD=$(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)
          SHELL
        end

Last, but not least, we have the creation of the last VM, where we run our application: 

	  #  #============
       config.vm.define "web" do |web|
          web.vm.box = "envimation/ubuntu-xenial"
          web.vm.hostname = "web"
          web.vm.network "private_network", ip: "192.168.33.12"
      
      # We want to access tomcat from the host using port 8080
          web.vm.network "forwarded_port", guest: 8080, host: 8080
      
       # We set more ram memmory for this VM
          web.vm.provider "virtualbox" do |v|
            v.memory = 1024
          end
        end
      end
___________________________________
#### Dockerfile - web 

We use to build a container according to the specifications placed here.
This Dockerfile is for web so we assure that the container have tomcat, git and node.

	FROM tomcat

	ARG SSH_PRIVATE_KEY

	ARG SSH_PUBLIC_KEY
	 
	RUN apt-get update -y
	 
	RUN apt-get install -f
	 
	RUN apt-get install git -y
	 
	RUN apt-get install nodejs -y
	 
	RUN apt-get install npm -y

	# Authorize SSH Host
	RUN mkdir -p /root/.ssh && \
	    chmod 0700 /root/.ssh && \
	    ssh-keyscan -t rsa bitbucket.org > /root/.ssh/known_hosts

	# Add the keys and set permissions
	RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa && \
	    echo "${SSH_PUBLIC_KEY}" > /root/.ssh/id_rsa.pub && \
	    chmod 600 /root/.ssh/id_rsa && \
	    chmod 600 /root/.ssh/id_rsa.pub && \
	    cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys

	RUN echo ${SSH_PRIVATE_KEY}

	RUN cat /root/.ssh/id_rsa

	RUN mkdir -p /tmp/build

	WORKDIR /tmp/build/

	RUN git clone git@bitbucket.org:BuckRogers_1/devops_g5.git

	WORKDIR /tmp/build/devops_g5/

	RUN chmod +x mvnw

	RUN ./mvnw compile war:war

	RUN cp target/training-1.0-SNAPSHOT.war  /usr/local/tomcat/webapps/

	RUN rm /root/.ssh/id_rsa

	RUN rm /root/.ssh/id_rsa.pub

	EXPOSE 8080
__________________________
#### db folder 

We use to build a container according to the specifications placed here.
This Dockerfile is for data base, so we assure that the container have the right access to our db.
The following file was added to this folder:

***dockerfile***

	FROM ubuntu

	RUN apt-get update && \

	apt-get install -y openjdk-8-jdk-headless && \

	apt-get install unzip -y && \

	apt-get install wget -y
	  
	RUN mkdir -p /usr/src/app
	  
	WORKDIR /usr/src/app/
	  
	RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
	  
	EXPOSE 8082

	EXPOSE 9092

	CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
___________________________________
#### jenkinsfile

When you configure Jenkins, in browser, you must install the following plugins:
- HTML publisher
- Jacoco
- Docker (pipeline, build step, API, plugin)

Execute the pipeline, to build the different stages that we want. 

		def PKEY
		def PUKEY

		node {
		  PKEY = sh(returnStdout: true, script: 'cat ssh/id_rsa')
		  PUKEY = sh(returnStdout: true, script: 'cat ssh/id_rsa.pub')
		}

		pipeline {
		    agent any
		    stages {
		            stage('Assemble') {
		                steps {
		                    echo 'Assembling...'
		                    sh 'chmod +x mvnw'
		                    sh './mvnw clean war:war'
		            }
		        }
		        stage('Test') {
		            steps {
		                echo 'Running tests...'
		                sh './mvnw test'
		                junit 'target/surefire-reports/*.xml'
		                publishHTML (target: [
		                  allowMissing: false,
		                  alwaysLinkToLastBuild: true,
		                  keepAll: true,
		                  reportDir: 'target/site/jacoco',
		                  reportFiles: 'index.html',
		                  reportName: "Tests Coverage"
		                ])
		            }
		        }
		        stage('Javadoc') {
		            steps {
		                echo 'Generating Javadoc...'
		                sh './mvnw javadoc:javadoc'
		                publishHTML (target: [
		                  allowMissing: false,
		                  alwaysLinkToLastBuild: true,
		                  keepAll: true,
		                  reportDir: 'target/site/apidocs',
		                  reportFiles: 'index.html',
		                  reportName: "Javadoc"
		                ])
		            }
		        }
		        stage('Archiving') {
		            steps {
		                echo 'Archiving...'
		                archiveArtifacts 'target/*.war'
		            }
		        }
		        stage('Checkout') {
		            steps {
		                echo 'Checking out...'
		                git credentialsId: 'Bitbucket', url:'https://BuckRogers_1@bitbucket.org/BuckRogers_1/devops_g5.git'
		            }
		        }
		        
		        stage('Docker Image') {
		            steps {
		                echo 'Creating and publishing Docker Image...'
		                script{
		                    docker.withRegistry('https://index.docker.io','dockerhub'){
		                        docker.build("1060237/switch_devops:${env.BUILD_ID}", "--build-arg SSH_PRIVATE_KEY='${PKEY}' --build-arg SSH_PUBLIC_KEY='${PUKEY}' .").push()
		                    }
		                }
		            }
		        }
		    }
		}

**Stages**:

Begin with the configuration of the ssh, then we enter in the the pipeline.

**1. Checkout:** Jenkins validate the Repository and it's credentials and get all the files in it.

**2. Assemble:** In order to prevent errors in the following 'Test' stage, we always remove the file with the results, 
thus adding "./mvnw clean war:war", in order to ensure that when we run the next stage it does not have any previous files there.

**3. Test:** run the tests associated to the program and put the results on jenkins.

**4. Javadoc:** generate the javadoc of the project and archive the results in a junit-formatted.

**5. Archive:** archive the files in the location we want.

**6. Docker:** generate a docker image with Tomcat and publish in docker hub.
____________________________________
#### playbook1.yml, playbook2.yml and jenkins.yml files ??????????

They were created to install jenkins using ansible..... 

_____________________________________
#### ansible.cfg file ????

When you configure Ansible, in a browser, you should install the following plugins:
- HTML publisher
- Jacoco
- Docker (pipeline, build step, API, plugin)
- Ansible (Tower, plugin)

After running the pipeline with success in all the stages we reconstruct the Vagrantfile, here we added:
- two clean VMs, one for web and another for productions.
- setup to install python and ansible.

This file create 4 VMs, two DB (one for production and another for tests), one for jenkins and one for web: 

	[defaults]
    inventory = hosts
    remote_user = vagrant
______________________________________

#### To conclude you must: ?????

##### 1- Clone the repo to a folder you want

##### 2- Prepare the VMs

2.1 - To create the VMs we run in command line:

    vagrant up
    
This will run the provision script to prepare the VMs, call the hypervisor and effectively create all the 4 VMs we defined, 
with the tools, setups the network and shared folders, and setups SSH keys to logon to the VMs.

2.2 - Then we wants to install Docker in the VM that have the Jankins and Ansible, that have the name VM Jenkins, so we run: 
    
    vagrant ssh jenkins

    sudo apt-get remove docker docker-engine docker.io containerd runc

    sudo apt-get update

    sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common
    
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    
    sudo apt-key fingerprint 0EBFCD88

The result of this is:

    pub   rsa4096 2017-02-22 [SCEA]
          9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
    uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
    sub   rsa4096 2017-02-22 [S]
    
    
    sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"

2.3 - Then we need to install Docker Engine:

    sudo apt-get update
    
    sudo apt-get install docker-ce docker-ce-cli containerd.io -y
    
    apt-cache madison docker-ce
    
This is the result, here we see some examples of containers that the Docker has:
    
      docker-ce | 5:18.09.1~3-0~ubuntu-xenial | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
      docker-ce | 5:18.09.0~3-0~ubuntu-xenial | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
      docker-ce | 18.06.1~ce~3-0~ubuntu       | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
      docker-ce | 18.06.0~ce~3-0~ubuntu       | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
      ...

We choose one and installed a specific version using the version string from the second column, for example, 5:18.09.1~3-0~ubuntu-xenial.
    
    sudo apt-get install docker-ce=5:18.09.1~3-0~ubuntu-xenial docker-ce-cli=5:18.09.1~3-0~ubuntu-xenial containerd.io
    
Then we run the following command to assure that the container was created, the result will be an image of that container:
    
    sudo docker run hello-world

2.4 - This step must always be done when you start the VM, here we manage Docker as a non-root user:

    sudo groupadd docker
    
    sudo usermod -aG docker $USER
    
    sudo chmod 666 /var/run/docker.sock
    
    sudo docker login
    
        result: 
        Authenticating with existing credentials...
        WARNING! Your password will be stored unencrypted in /home/vagrant/.docker/config.json.
        Configure a credential helper to remove this warning. See
        https://docs.docker.com/engine/reference/commandline/login/#credentials-store
        
         
        
        Login Succeeded 

##### Build the project

##### See the results
............
_____________________________________
## ALTERNATIVE
____________________________________

URL do repo
___________________________________

###  Work Division 

___________________________________
